import argparse
import json
import os

import waitress
from flask import Flask
from flask import jsonify


DOMAIN_DELIMITER = '.'

ALIAS_DOMAINS = "domains"
ALIAS_FILES = "files"
ALIAS_STICKERS = "stickers"


app = Flask(__name__)


aliases = {}


def load_data():
    """Loads aliases data from file"""
    with open('data/aliases.json', 'r') as s:
        aliases.update(json.load(s))


load_data()


def _find_domain_image(domain):
    levels = domain.split(DOMAIN_DELIMITER)
    while len(levels) > 1:
        domain = DOMAIN_DELIMITER.join(levels)

        image = aliases[ALIAS_DOMAINS].get(domain)
        if image:
            return image
        else:
            del levels[0]


def _wrap_result(data):
    return jsonify(data)


def _get_last_update():
    return os.path.getctime('data/aliases.json')


@app.route('/domains/image/<domain>', methods=['GET'])
def domain_image(domain):
    image_name = _find_domain_image(domain)
    if image_name:
        data = {
            'name': image_name
        }
        return _wrap_result(data)
    else:
        return '', 404


@app.route('/files/image/<path:mimetype>', methods=['GET'])
def file_image(mimetype):
    image_name = aliases[ALIAS_FILES].get(mimetype)
    if image_name:
        data = {
            'name': image_name
        }
        return _wrap_result(data)
    else:
        return '', 404


@app.route('/stickers/<query>', methods=['GET'])
def search_stickers(query):
    data = []

    for filename in aliases[ALIAS_STICKERS]:
        if query in filename:
            data.append({
                'name': filename
            })
    return _wrap_result(data)


@app.route('/sticker/<path:name>', methods=['GET'])
def get_sticker(name):
    filename = aliases[ALIAS_STICKERS].get(name)
    if filename:
        data = {
            'name': filename
        }
        return _wrap_result(data)
    else:
        return '', 404


@app.route('/last-update')
def last_update():
    return _wrap_result({'time': _get_last_update()})


@app.route('/health', methods=["GET"])
def health():
    return '', 204


@app.route('/update', methods=["POST"])
def update():
    load_data()
    return '', 204


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Neboard external images service')
    parser.add_argument('--port', type=int, help='Port to run on', default=8000)

    args = parser.parse_args()

    threads = os.environ.get('THREADS')
    if threads:
        threads = int(threads)
    else:
        threads = 4

    waitress.serve(app, port=args.port, threads=threads)
