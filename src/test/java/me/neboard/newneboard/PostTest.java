package me.neboard.newneboard;

import me.neboard.newneboard.service.impl.PostCreateDetails;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.parser.ParserResult;
import me.neboard.newneboard.repository.AttachmentRepository;
import me.neboard.newneboard.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;

@AutoConfigureTestEntityManager
@RunWith(SpringRunner.class)
public class PostTest extends NewneboardApplicationTests {
    private static final String TEST_TAG = "tag";
    private static final String TEST_TITLE = "title";
    private static final String TEST_TEXT = "text";

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PostService postService;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private AttachmentRepository attachmentRepository;

    @MockBean
    private ParserService mockParser;

    @Before
    public void mockTextParser() throws BusinessException {
        ParserResult mockParserResult = new ParserResult();
        mockParserResult.setText(TEST_TEXT);
        mockParserResult.setReflinks(Collections.emptyList());

        Mockito.when(mockParser.preparse(anyString())).thenReturn(TEST_TEXT);
        Mockito.when(mockParser.parse(anyString())).thenReturn(mockParserResult);
    }

    @Test
    public void createPostWithoutAttachments() throws BusinessException {
        PostCreateDetails details = new PostCreateDetails(TEST_TITLE, TEST_TEXT,
                Collections.emptyList(), new String[] {TEST_TAG}, null, null);
        postService.create(details);

        Page<PostThread> allThreads = threadService.findAll(utilService.getPageable());
        assertEquals(1, allThreads.getNumberOfElements());
        PostThread thread = allThreads.getContent().get(0);

        assertEquals(1, postService.getCountByThread(thread));
        assertEquals(1, thread.getTags().size());
        assertEquals(TEST_TAG, thread.getTags().iterator().next().getName());
    }

    @Test
    public void getSpeed() throws BusinessException {
        PostCreateDetails details = new PostCreateDetails(TEST_TITLE, TEST_TEXT,
                Collections.emptyList(), new String[] {TEST_TAG}, null, null);
        postService.create(details);

        assertTrue(postService.getPostSpeed() > 0);
    }

    @Test
    public void createPostWithAttachments() throws BusinessException {
        Attachment urlAttachment = new Attachment(null, "http://example.com/");
        attachmentService.save(urlAttachment);

        List<Attachment> attachments = new ArrayList<>();
        attachments.add(urlAttachment);

        PostCreateDetails details = new PostCreateDetails(TEST_TITLE, TEST_TEXT,
                attachments, new String[] {TEST_TAG}, null, null);
        postService.create(details);

        Page<PostThread> allThreads = threadService.findAll(utilService.getPageable());
        assertEquals(1, allThreads.getNumberOfElements());
        PostThread thread = allThreads.getContent().get(0);

        assertEquals(1, postService.getCountByThread(thread));
        assertEquals(1, thread.getTags().size());
        assertEquals(TEST_TAG, thread.getTags().iterator().next().getName());
    }

    @Test
    public void findAttachmentsWithoutPosts() throws BusinessException {
        Attachment urlAttachment = new Attachment(null, "http://example.com/");
        attachmentService.save(urlAttachment);

        List<Attachment> attachments = new ArrayList<>();
        attachments.add(urlAttachment);

        PostCreateDetails details = new PostCreateDetails(TEST_TITLE, TEST_TEXT,
                attachments, new String[] {TEST_TAG}, null, null);
        Post post = postService.create(details);

        assertTrue(attachmentRepository.findWithoutPosts().isEmpty());

        postService.delete(post);

        assertFalse(attachmentRepository.findWithoutPosts().isEmpty());
    }

}
