package me.neboard.newneboard.service;

import me.neboard.newneboard.NewneboardApplicationTests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@AutoConfigureTestEntityManager
@RunWith(SpringRunner.class)
public class UtilServiceTest extends NewneboardApplicationTests {
    @Autowired
    private UtilService utilService;

    @Autowired
    private TripcodeService tripcodeService;

    @Test
    public void testIsImage() {
        assertTrue(utilService.isImage("image/jpeg"));
        assertTrue(utilService.isImage("image/png"));
        assertTrue(utilService.isImage("image/gif"));
        assertTrue(utilService.isImage("image/webp"));
        assertTrue(utilService.isImage("image/bmp"));
    }

    @Test
    public void testEncodeTripcode() {
        assertEquals("098F6BCD4621D373CADE4E832627B4F6", tripcodeService.encodeTripcode("test"));
    }
}
