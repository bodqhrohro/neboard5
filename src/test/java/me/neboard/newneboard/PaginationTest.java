package me.neboard.newneboard;

import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.UtilService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

@AutoConfigureTestEntityManager
@RunWith(SpringRunner.class)
public class PaginationTest extends NewneboardApplicationTests {
    private static final int PER_PAGE = 1;

    @Autowired
    private UtilService utilService;

    @Test
    public void onlyLeftPagination() throws BusinessException {
        List<Void> elements = getListOf(10);

        PageImpl<Void> page = new PageImpl<>(elements, getPageable(0), elements.size());
        assertEquals(getExpectedString(0, Arrays.asList(0, 1, 2, 7, 8, 9)),
                utilService.getPaginatorBar(page, getMockRequest()));
    }

    @Test
    public void wholePaginationNoDividers() throws BusinessException {
        List<Void> elements = getListOf(10);

        PageImpl<Void> page = new PageImpl<>(elements, getPageable(4), elements.size());
        assertEquals(getExpectedString(4, Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)),
                utilService.getPaginatorBar(page, getMockRequest()));
    }

    @Test
    public void onlyRightPagination() throws BusinessException {
        List<Void> elements = getListOf(10);

        PageImpl<Void> page = new PageImpl<>(elements, getPageable(9), elements.size());
        assertEquals(getExpectedString(9, Arrays.asList(0, 1, 2, 7, 8, 9)),
                utilService.getPaginatorBar(page, getMockRequest()));
    }

    private Pageable getPageable(int pageNumber) {
        return utilService.getPageable(Optional.of(pageNumber), PER_PAGE);
    }

    private List<Void> getListOf(int elementsNumber) {
        return IntStream
                .range(0, elementsNumber)
                .<Void>mapToObj(i -> null)
                .collect(Collectors.toList());
    }

    private HttpServletRequest getMockRequest() {
        return new MockHttpServletRequest(HttpMethod.GET.toString(), "/");
    }

    private String getExpectedString(int currentPage, Collection<Integer> pages) {
        return pages
                .stream()
                .map(page -> currentPage == page
                        ? String.format("<a href='/?page=%d' class='current_page'>%d</a>", page, page)
                        : String.format("<a href='/?page=%d'>%d</a>", page, page))
                .collect(Collectors.joining(" "));
    }

}
