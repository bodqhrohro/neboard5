var MIN_INPUT_LENGTH = 2;
var URL_TAGS = '/api/tags';
var TAG_DELIMITER = ' ';

function split( val ) {
    return val.split(TAG_DELIMITER);
}

function extractLast( term ) {
    return split( term ).pop();
}

$( document ).ready(function() {
    $('#tags').autocomplete({
        source: function( request, response ) {
            $.getJSON(URL_TAGS, {
                term: extractLast( request.term )
            }, response);
        },
        search: function() {
            // custom minLength
            var term = extractLast( this.value );
            if (term.length < MIN_INPUT_LENGTH) {
                return false;
            }
        },
        focus: function() {
            // prevent value inserted on focus
            return false;
        },
        select: function( event, ui ) {
            var terms = split( this.value );
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push( ui.item.value );
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(TAG_DELIMITER);
            return false;
        },
        open: function(event, ui) {
			$(this).autocomplete("widget").css({
				"width": ($(this).width() + "px")
			});
		}
    });

    initAjaxForm(null);
});