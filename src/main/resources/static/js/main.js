/*
 @licstart  The following is the entire license notice for the
 JavaScript code in this page.


 Copyright (C) 2013  neko259

 The JavaScript code in this page is free software: you can
 redistribute it and/or modify it under the terms of the GNU
 General Public License (GNU GPL) as published by the Free Software
 Foundation, either version 3 of the License, or (at your option)
 any later version.  The code is distributed WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

 As additional permission under GNU GPL version 3 section 7, you
 may distribute non-source (e.g., minimized or compacted) forms of
 that code without the copy of the GNU GPL normally required by
 section 4, provided you include this license notice and a URL
 through which recipients can access the Corresponding Source.

 @licend  The above is the entire license notice
 for the JavaScript code in this page.
 */

var ITEM_VOLUME_LEVEL = 'volumeLevel';
var ITEM_HIDDEN_POSTS = 'hiddenPosts';

var IMAGE_TYPES = ['image/png', 'image/jpg', 'image/jpeg', 'image/bmp', 'image/gif'];

function gettext(key) {
    // TODO Figure out how to get localized message
    return key;
}

/**
 * Highlight code blocks with code highlighter
 */
function highlightCode(node) {
    node.find('pre code').each(function(i, e) {
//        hljs.highlightBlock(e);
    });
}

function updateFavPosts(data) {
    var includePostBody = $('#fav-panel').is(":visible");

    var allNewPostCount = 0;

    if (includePostBody) {
        var favoriteThreadPanel = $('#fav-panel');
        favoriteThreadPanel.empty();
    }

    $.each($.parseJSON(data), function (_, dict) {
        var newPostCount = dict.new_post_count;
        allNewPostCount += newPostCount;

        if (includePostBody) {
            var favThreadNode = $('<div class="post"></div>');
            favThreadNode.append($(dict.post_url));
            favThreadNode.append(' ');
            favThreadNode.append($('<span class="title">' + dict.title + '</span>'));

            if (newPostCount > 0) {
                favThreadNode.append(' (<a href="' + dict.newest_post_link + '">+' + newPostCount + "</a>)");
            }

            favoriteThreadPanel.append(favThreadNode);

            addRefLinkPreview(favThreadNode[0]);
        }
    });

    var newPostCountNode = $('#new-fav-post-count');
    if (allNewPostCount > 0) {
        newPostCountNode.text('(+' + allNewPostCount + ')');
        newPostCountNode.show();
    } else {
        newPostCountNode.hide();
    }
}

function initFavPanel() {
    var favPanelButton = $('#fav-panel-btn');
    if (favPanelButton.length > 0 && typeof SharedWorker != 'undefined') {
        var worker = new SharedWorker($('body').attr('data-update-script'));
        worker.port.onmessage = function(e) {
            updateFavPosts(e.data);
        };
        worker.onerror = function(event){
            throw new Error(event.message + " (" + event.filename + ":" + event.lineno + ")");
        };
        worker.port.start();

        $(favPanelButton).click(function() {
            var favPanel = $('#fav-panel');
            favPanel.toggle();

            worker.port.postMessage({ includePostBody: favPanel.is(':visible')});

            return false;
        });

        $(document).on('keyup.removepic', function(e) {
            if(e.which === 27) {
                $('#fav-panel').hide();
            }
        });
    }
}

function setVolumeLevel(level) {
    localStorage.setItem(ITEM_VOLUME_LEVEL, level);
}

function getVolumeLevel() {
    var level = localStorage.getItem(ITEM_VOLUME_LEVEL);
    if (level == null) {
        level = 1.0;
    }
    return level
}

function processVolumeUser(node) {
    if (!window.localStorage) return;
    node.prop("volume", getVolumeLevel());
    node.on('volumechange', function(event) {
        setVolumeLevel(event.target.volume);
        $("video,audio").prop("volume", getVolumeLevel());
    });
}

/**
 * Add all scripts than need to work on post, when the post is added to the
 * document.
 */
function addScriptsToPost(post) {
    addRefLinkPreview(post[0]);
    highlightCode(post);
    processVolumeUser(post.find("video,audio"));

    if (typeof initAdminButtons !== 'undefined') {
    	initAdminButtons(post);
    }

    operaMiniDelegationFix(post);
}

/**
 * Fix compatibility issues with some rare browsers
 */
function compatibilityCrutches() {
    if (window.operamini) {
        $('#form textarea').each(function() { this.placeholder = ''; });
        $('#form .file-picker').hide();
        $('#form input[type="file"]').show();
        $('.thread .post').each(function() { operaMiniDelegationFix($(this)); });
    }
}

function operaMiniDelegationFix(post) {
    if (window.operamini) {
        // Because of the anchor link, Opera Mini's delegation detector
        // doesn't trigger there, so working this around with a dummy
        // handler
        post.find('.quick-reply').each(function() {
            $(this).on('click', function() { return true; });
        });
    }
}



function createAndSubmitAdminActionForm(action, post) {
    var form = document.createElement("form");
    var postIdInput = document.createElement("input");

    form.method = "POST";
    form.action = action;

    postIdInput.value=post.attr('id');
    postIdInput.name="postId";
    postIdInput.type="hidden"
    form.appendChild(postIdInput);

    document.body.appendChild(form);

    form.submit();
}

function refreshPageData() {
    if (typeof getThreadDiff === "function") {
        getThreadDiff();
    } else {
        location.reload();
    }
}

function addAdminContextMenu() {
	$.contextMenu({
		selector: '.admin-post-context-menu',
		items: {
			"edit": {name: "Edit"},
			"sep1": "---------",
			"ban": {name: "Ban"},
			"sep2": "---------",
			"delete": {name: "Delete"},
			"purge": {name: "Purge"},
		},
		callback: function(key, options) {
            var post = $(this).parents('.post');
            var postId = post.attr('id');

			switch (key) {
				case 'edit':
                    location.href = (post.data('opening') ? '/admin/edit/thread/' : '/admin/edit/post/') + postId;
					break
				case 'delete':
				    if (confirm(gettext('Do you really want to delete this?'))) {
                        $.ajax({
                            type: "POST",
                            url: '/api/post/' + postId + '/delete',
                            success: function(data) {
                                if (data.url != null) {
                                    location.href = data.url;
                                } else {
                                    refreshPageData();
                                }
                            },
                            dataType: 'json'
                        });
				    }
				    break
				case 'ban':
				    if (confirm(gettext('Do you really want to ban the user?'))) {
				        var address = $(this).attr('title');
                        $.ajax({
                            type: "POST",
                            url: '/api/admin/ban/',
                            data: {
                                postId: post.attr('id'),
                                address: address
                            },
                            success: function() {
                                alert('User ' + address + ' banned')
                            },
                            dataType: 'json'
                        });
				    }
				    break
				case 'purge':
				    if (confirm(gettext('Do you really want to ban the author and delete his posts earlier than this one?'))) {
    				    createAndSubmitAdminActionForm('/api/' + $(this).attr('title') + '/purge/', post);
    				}
				    break;
			}
		}
	});
}

$( document ).ready(function() {
    $("a[href='#top']").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    addImgPreview();

    addRefLinkPreview();

    initFavPanel();

    var volumeUsers = $("video,audio");
    processVolumeUser(volumeUsers);

    compatibilityCrutches();
    addAdminContextMenu();
});
