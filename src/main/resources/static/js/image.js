/*
 @licstart  The following is the entire license notice for the
 JavaScript code in this page.


 Copyright (C) 2013  neko259

 The JavaScript code in this page is free software: you can
 redistribute it and/or modify it under the terms of the GNU
 General Public License (GNU GPL) as published by the Free Software
 Foundation, either version 3 of the License, or (at your option)
 any later version.  The code is distributed WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

 As additional permission under GNU GPL version 3 section 7, you
 may distribute non-source (e.g., minimized or compacted) forms of
 that code without the copy of the GNU GPL normally required by
 section 4, provided you include this license notice and a URL
 through which recipients can access the Corresponding Source.

 @licend  The above is the entire license notice
 for the JavaScript code in this page.
 */

var IMAGE_POPUP_MARGIN = 10;


var IMAGE_VIEWERS = [
    ['simple', new SimpleImageViewer()],
    ['popup', new PopupImageViewer()]
];

var CLASS_FULL_IMG = 'post-image-full';
var SELECTOR_CLASS_POPUP = '.img-full';

var SCALE_DEFAULT = 1;


// Init image viewer
var viewerName = $('header').attr('data-image-viewer');
var viewer = ImageViewer();
for (var i = 0; i < IMAGE_VIEWERS.length; i++) {
    var item = IMAGE_VIEWERS[i];
    if (item[0] === viewerName) {
        viewer = item[1];
        break;
    }
}


function getFullImageWidth(previewImage) {
    var full_img_w = previewImage.attr('data-width');
    if (full_img_w == null) {
        full_img_w = previewImage[0].naturalWidth;
    }

    return full_img_w;
}

function getFullImageHeight(previewImage) {
    var full_img_h = previewImage.attr('data-height');
    if (full_img_h == null) {
        full_img_h = previewImage[0].naturalHeight;
    }

    return full_img_h;
}


function ImageViewer() {}
ImageViewer.prototype.view = function (post) {};

function SimpleImageViewer() {}
SimpleImageViewer.prototype.view = function (post) {
    var images = post.find('img');
    images.toggle();

    // When we first enlarge an image, a full image needs to be created
    if (images.length == 1) {
        var thumb = images.first();

        var width = getFullImageWidth(thumb);
        var height = getFullImageHeight(thumb);

        if (width == null || height == null) {
            width = '100%';
            height = '100%';
        }

        var parent = images.first().parent();
        var link = parent.attr('href');

        var fullImg = $('<img />')
            .addClass(CLASS_FULL_IMG)
            .attr('src', link)
            .attr('width', width)
            .attr('height', height);

        parent.append(fullImg);
    }
};

function PopupImageViewer() {}
PopupImageViewer.prototype.view = function (post) {
    var el = post;
    var thumb_id = 'full' + el.find('img').attr('alt');

    var existingPopups = $('#' + thumb_id);
    if (!existingPopups.length) {
        var imgElement = el.find('img');

        var exifOrientation = imgElement.attr('data-orientation');
        var rotation = null;
        var invertDimensions = false;
        if (exifOrientation == 8) {
            rotation = 270;
            invertDimensions = true;
        } else if (exifOrientation == 3) {
            rotation = 180;
        } else if (exifOrientation == 6) {
            rotation = 90;
            invertDimensions = true;
        }

        var full_img_w = getFullImageWidth(imgElement);
        var full_img_h = getFullImageHeight(imgElement);

        var win = $(window);

        var win_w = win.width();
        var win_h = win.height();

        // New image size
        var freeWidth = win_w - 2 * IMAGE_POPUP_MARGIN;
        var freeHeight = win_h - 2 * IMAGE_POPUP_MARGIN;

        var orientedFullWidth = invertDimensions ? full_img_h : full_img_w;
        var orientedFullHeight = invertDimensions ? full_img_w : full_img_h;

        var w_scale;
        var h_scale;
        if (orientedFullWidth > freeWidth) {
            w_scale = orientedFullWidth / freeWidth;
        } else {
        	w_scale = SCALE_DEFAULT;
        }
        if (orientedFullHeight > freeHeight) {
            h_scale = orientedFullHeight / freeHeight;
        } else {
        	h_scale = SCALE_DEFAULT;
        }

        var scale = Math.max(w_scale, h_scale)

        var img_w = full_img_w / scale;
        var img_h = full_img_h / scale;

        var postNode = $(el);

        var img_pv = new Image();
        var newImage = $(img_pv);
        if (rotation) {
            newImage.css({'transform': 'rotate(' + rotation + 'deg)'});
        }
        newImage.addClass('img-full')
            .attr('id', thumb_id)
            .attr('src', postNode.attr('href'))
            .css({
                'width': img_w,
                'height': img_h,
                'left': (win_w - img_w) / 2,
                'top': (win_h - img_h) / 2,
                'position': 'fixed' // Chrome fix because it adds a 'relative' value to the image somehow
            })
            //scaling preview
            .mousewheel(function(event, delta) {
                var cx = invertDimensions ? event.originalEvent.clientY : event.originalEvent.clientX;
                var cy = invertDimensions ? event.originalEvent.clientX : event.originalEvent.clientY;

                var scale = delta > 0 ? 1.25 : 0.8;

                // Set position
                var oldPosition = newImage.position();

                newImage.css({
                	'width': newImage.width() * scale,
                	'height': newImage.height() * scale,
                    'left': parseInt(cx - (cx - parseInt(oldPosition.left, 10)) * scale, 10),
                    'top':  parseInt(cy - (cy - parseInt(oldPosition.top, 10)) * scale, 10)
                });

                return false;
            })
            .draggable({
                addClasses: false,
                stack: SELECTOR_CLASS_POPUP
            })
            .appendTo($('body'));
            newImage.click(function() {
                $(this).remove();
            });
    } else {
        existingPopups.remove();
    }
};

function addImgPreview() {
    //keybind
    $(document).on('keyup.removepic', function(e) {
        if(e.which === 27) {
            $(SELECTOR_CLASS_POPUP).remove();
        }
    });

    $('body').on('click', '.thumb', function() {
        viewer.view($(this));

        return false;
    });
}
