var ITEM_FILE_SOURCE = 'fileSource';
var URL_STICKERS = '/api/stickers/';
var MIN_INPUT_LENGTH = 3;
var URL_DELIMITER = '\n';
var REPLY_TO_MSG = '.reply-to-message';
var REGEX_HOST_POST = new RegExp('https?://' + window.location.hostname + "(:\\d+)?/thread/(\\d+)(/?#(\\d+))?/?");
var REGEX_HOST_ATTACHMENT = new RegExp('https?://' + window.location.hostname + "(:\\d+)?/files/(.+)/?");
var FORM_FADE_DELAY = 300;
var PREFIX_ATTACHMENT = "attachment://";

var PLACEHOLDER_URL = '/images/url.png';
var PLACEHOLDER_FILE = '/images/file.png';
var PLACEHOLDER_INTERNAL = '[INTERNAL]';

var pastedFiles = [];

var submitInProgress = false;

/**
 * Show text in the errors row of the form.
 * @param form
 * @param text
 */
function showAsErrors(form, errors) {
    form.children('.form-errors').remove();

    if (errors != null) {
        for (var i = 0; i < errors.length; i++) {
            var errorList = $('<div class="form-errors">(!) ' + errors[i] + '<div>');
            errorList.appendTo(form);
        }
    }
}

function getPostTextarea(form) {
    return form.find('textarea[name=text]');
}

function addPreviewImage(form, imageSrc, name) {
    var fileNode = $('<div class="file-preview" />')
    var img = $('<img class="image-preview" />');
    img.attr('src', imageSrc);

    fileNode.append(img);
    if (name != null) {
        fileNode.append($('<div class="file-preview-name">' + name + '</div>'));
    }

    $('.pasted-images').append(fileNode);

    fileNode.on("click", function() {
        // Remove the image from all lists
        var itemIndex = $(this).index();
        pastedFiles.splice(itemIndex, 1);
        $(this).remove();
    });
}


function addBlobToForm(blob, name, form) {
    if (blob == null) {
        // Pasted images cannot be read sometimes
        return false;
    }

    var fileReader = new FileReader();

    var newIndex = pastedFiles.push({ blob: blob }) - 1;

    var defer = $.Deferred();

    fileReader.onload = function() {
        var imageSrc;
        if (blob.type.match('image.*')) {
            var resultArray = new Uint8Array(fileReader.result);
            var binaryString = '';

            for (var i = 0; i < resultArray.length; i++) {
                binaryString += String.fromCharCode(resultArray[i]);
            }

            imageSrc = 'data:' + blob.type + ';base64,' + window.btoa(binaryString);
        } else {
            imageSrc = PLACEHOLDER_FILE;
        }

        addPreviewImage(form, imageSrc, name);

        // Clone the blob, so its parameters won't get
        // messed up when the file input is reused
        pastedFiles[newIndex] = {
            name: blob.name,
            blob: new Blob([fileReader.result], { type: blob.type })
        };

        defer.resolve();
    };

    fileReader.readAsArrayBuffer(blob);

    return defer;
}

function addFilesToFormSequentially(files, form) {
    var defer = $.Deferred();
    var chain = defer;

    $.each(files, function(index, file) {
        chain = chain.then(function() {
            return addBlobToForm(file, file.name, form);
        });
    });

    defer.resolve();
}

function addUrlToForm(url, form) {
    var imageSrc;
    var name;

    var matcher = url.startsWith(PREFIX_ATTACHMENT);
    if (matcher) {
        imageSrc = url.replace(PREFIX_ATTACHMENT, '/files/');
        name = PLACEHOLDER_INTERNAL;
    } else {
        imageSrc = PLACEHOLDER_URL;
        name = url;
    }

    pastedFiles.push({ url: url });

    addPreviewImage(form, imageSrc, name);
}

function addOnImagePaste(form) {
    $(document).on('paste', function(event) {
        var items = (event.clipboardData || event.originalEvent.clipboardData).items;
        for (index in items) {
            var item = items[index];
            if (item.kind === 'file') {
                var blob = item.getAsFile();

                addBlobToForm(blob, null, form);
            }
        }
    });
}

function addOnFileAdded(form) {
    form.find('.file-picker').click(function(){
       form.find("input[name=attachments]").trigger('click');
    })

    var attachments = form.find('input[name=attachments]');
    attachments.on('change', function(event) {
        addFilesToFormSequentially(event.target.files, form);

        // Clean up form input since we saved the file blob elsewhere
        attachments.val('');
    });
}

function addOnDrag(form) {
    form.on('dragenter', function(event) {
        event.preventDefault();
        event.stopPropagation();
    });
    form.on('dragover', function(event) {
        event.preventDefault();
        event.stopPropagation();
    });
    form.on('drop', function(event) {
        if (event.originalEvent.dataTransfer) {
            event.preventDefault();
            event.stopPropagation();

            addFilesToFormSequentially(event.originalEvent.dataTransfer.files, form);
        }
    });
}

function addOnLinkPaste(form) {
    getPostTextarea(form).on('paste', function(event) {
        var data = (event.clipboardData || event.originalEvent.clipboardData).getData('text');

        var textarea = getPostTextarea(form)[0];

        var matcher = data.match(REGEX_HOST_POST);
        if (matcher) {
			var newText = '>>' + (matcher[4] || matcher[2]);

			insertTextToTextarea(newText, textarea);

	        return false;
        }
    });

    var attachmentsTextarea = form.find('textarea[name=attachment_urls]');
    attachmentsTextarea.on('paste', function(event) {
        var data = (event.clipboardData || event.originalEvent.clipboardData).getData('text');

        var textarea = attachmentsTextarea[0];

        var matcher = data.match(REGEX_HOST_ATTACHMENT);
        if (matcher) {
            var newText = "attachment://" + matcher[2];

            insertTextToTextarea(newText, textarea);

            return false;
        }
    });
}

function unlockForm() {
    $('#overlay').fadeOut(FORM_FADE_DELAY);
    submitInProgress = false;
}

/**
 * When the form is posted, this method will be run as a callback
 */
function updateOnPost(response, statusText, xhr, form) {
    var url = response.url;

    showAsErrors(form, null);
    unlockForm();

    if (url) {
        // New thread
        document.location = url;
    } else {
        // Update thread
        completeThreadUpdate();
        resetForm();
        getThreadDiff();
        scrollToBottom();
    }
}

function initAjaxForm(openingPostId) {
    var form = $('#form');

    var url = '/api/post/';
    if (openingPostId) {
        url += openingPostId + '/';
    }

    if (form.length > 0) {
        var options = {
            beforeSubmit: function(arr, form, options) {
                if (submitInProgress) {
                    // Prevent double posting
                    return false;
                }

                submitInProgress = true;

                $('#overlay').fadeIn(FORM_FADE_DELAY);

                var formData = new FormData();
                var urlsField;

                $.each(arr, function(i, item) {
                    // Skip empty file elements if no file is selected
                    if ( !(item.type == "file" && item.value == "") ) {
                        if (item.name === 'attachment_urls') {
                            urlsField = item;
                        } else {
                            formData.append(item.name, item.value);
                        }
                    }
                });

                $.each(pastedFiles, function(i, file) {
                    if (file.blob) {
                        formData.append('attachments', file.blob, file.name);
                    } else {
                        if (urlsField) {
                            urlsField.value = urlsField.value + '\n' + file.url;
                        }
                    }
                });

                if (urlsField) {
                    formData.append(urlsField.name, urlsField.value);
                }

                options.formData = formData;
            },
            success: updateOnPost,
            error: function(xhr, textStatus, errorString) {
                var errors;
                if (xhr.responseJSON) {
                    errors = xhr.responseJSON.errors;
                } else {
                    var errorText = 'Server error ' + xhr.status + ' : ' + textStatus
                    if (errorString) {
                        errorText += ' / ' + errorString;
                    }
                    errors = [errorText];
                }

                showAsErrors(form, errors);
                unlockForm();
            },
            url: url
        };

        form.ajaxForm(options);
    }
}

function getForm() {
    return $('.post-form-w');
}

/**
 * Clear all entered values in the form fields
 */
function resetForm() {
    var form = getForm();

    form.find('input:text, input:password, input:file, textarea').val('');
    pastedFiles = [];
    $('.preview-text').hide();
    $('.form-errors').remove();
    $('.pasted-images').empty();

    resetFormPosition(form);
}

function resetFormPosition(form) {
    form.insertBefore($('footer'));

    $(REPLY_TO_MSG).hide();
}

function enableStickerAutocomplete(form) {
    function split( val ) {
        return val.split(URL_DELIMITER);
    }

    function extractLast( term ) {
        return split(term).pop();
    }

    form.find('textarea[name=attachment_urls]').autocomplete({
        source: function( request, response ) {
            $.getJSON(URL_STICKERS + extractLast(request.term), {}, function (data) {
                response($.map(data.data, function (value, key) {
                    return {
                        url: '/files/' + value.name,
                        name: value.name
                    };
                }));
            });
        },
        search: function() {
            // custom minLength
            var term = extractLast( this.value );
            if (term.length < MIN_INPUT_LENGTH) {
                return false;
            }
        },
        focus: function() {
            // prevent value inserted on focus
            return false;
        },
        select: function( event, ui ) {
            var terms = split( this.value );
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push("sticker://" + ui.item.name);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(URL_DELIMITER);
            return false;
        }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
            .append( "<div>" + '<img class="sticker-preview" src="' + item.url + '">' + '<br />' + item.name + "</div>" )
            .appendTo( ul );
    };
}

function initPreviewButton(form) {
    form.find('#preview-button').click(function() {
        var linksInput = form.find('textarea[name=attachment_urls]');
        $.each(linksInput.val().split('\n'), function(index, url) {
            if (url) {
                addUrlToForm(url, form);
            }
        })
        linksInput.val('');

        var data = {
            raw_text: getPostTextarea(form).val()
        }

        var diffUrl = '/api/preview/';

        $.post(diffUrl,
            data,
            function(data) {
                var previewTextBlock = form.find('.preview-text');
                previewTextBlock.html(data);
                previewTextBlock.show();

                addScriptsToPost(previewTextBlock);
            })
    });
}

function enableSubmitHotkey(form) {
    form.find('textarea').keypress(function(event) {
        if ((event.which == 10 || event.which == 13) && event.ctrlKey) {
            form.find('[type=submit]').click();
            return false;
        }
    });
}


function initPanelButtons(form) {
    $('#mark-panel').on('click', '.mark_btn', function() {
        var button = $(this);
        addMarkToMsg(form, button.data('tag-left'), button.data('tag-right'),
            button.data('needs-input'), button.data('input-hint'));
    });
}

function insertTextToTextarea(text, textarea) {
    var startPos = textarea.selectionStart;
    var endPos = textarea.selectionEnd;

    var oldValue = textarea.value;
    textarea.value = oldValue.substring(0, startPos) + text
            + oldValue.substring(endPos, oldValue.length);

    var newPos = startPos + text.length;
    textarea.setSelectionRange(newPos, newPos);
}

$(document).ready(function() {
    $('.post-form').each(function() {
        var form = $(this);

        addOnImagePaste(form);
        addOnFileAdded(form);
        addOnLinkPaste(form);
        addOnDrag(form);

        enableStickerAutocomplete(form);
        initPreviewButton(form);
        initPanelButtons(form);
        enableSubmitHotkey(form);
    });
});
