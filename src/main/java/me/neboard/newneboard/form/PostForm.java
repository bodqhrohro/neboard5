package me.neboard.newneboard.form;

import me.neboard.newneboard.creation.attachment.FormAttachment;
import me.neboard.newneboard.creation.attachment.MultipartFileFormAttachment;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

public class PostForm implements CreatableForm {
	private String title;

	private String text;

	private List<MultipartFile> attachments = new ArrayList<>();

	private String attachment_urls;

	private String tripcode;

	private List<FormAttachment> processedAttachments = new ArrayList<>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<MultipartFile> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<MultipartFile> attachments) {
		this.attachments = attachments;
	}

	public String getAttachment_urls() {
		return attachment_urls;
	}

	public void setAttachment_urls(String attachment_urls) {
		this.attachment_urls = attachment_urls;
	}

	public String getTripcode() {
		return tripcode;
	}

	public void setTripcode(String tripcode) {
		this.tripcode = tripcode;
	}

	public List<FormAttachment> getProcessedAttachments() {
		return processedAttachments;
	}

	@Override
	public String[] getTagNames() {
		return null;
	}

	public void addProcessedAttachment(FormAttachment attachment) {
		processedAttachments.add(attachment);
	}

}
