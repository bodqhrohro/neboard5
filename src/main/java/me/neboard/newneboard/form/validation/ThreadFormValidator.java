package me.neboard.newneboard.form.validation;

import me.neboard.newneboard.form.ThreadForm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ThreadFormValidator extends PostFormValidator {
    private static final String ERROR_TAGS_REQUIRED = "error.tags.required";
    private static final String ERROR_TAGS_INVALID = "error.tags.invalid";
    private static final String ERROR_TAGS_LENGTH = "error.tags.length";

    private static final Pattern REGEX_TAGS = Pattern.compile("^[\\w\\s\\d\']+$", Pattern.UNICODE_CHARACTER_CLASS);

    @Value("${tags.max.length}")
    private int tagsMaxLength;

    @Override
    public boolean supports(Class<?> clazz) {
        return ThreadForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);

        ThreadForm form = (ThreadForm) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tags", ERROR_TAGS_REQUIRED);
        if (form.getTags().length() > tagsMaxLength) {
            errors.rejectValue("tags", ERROR_TAGS_LENGTH);
        }
        Matcher tagMatcher = REGEX_TAGS.matcher(form.getTags());
        if (!tagMatcher.matches()) {
            errors.rejectValue("tags", ERROR_TAGS_INVALID);
        }
    }
}
