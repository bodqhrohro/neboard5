package me.neboard.newneboard.form.download;

import me.neboard.newneboard.exception.BusinessException;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.metadata.Metadata;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Pattern;

@Component
public class UrlDownloader implements Downloader {
    private static final Pattern REGEX_URL = Pattern.compile("^https?://.+$");

    private static final int DOWNLOAD_BUFFER_SIZE = 4096;
    private static final String TEMP_FILE_PREFIX = "attach";

    public static final String EXTENSION_DELIMITER = ".";
    private static final Pattern REGEX_EXTENSION_DELIMITER = Pattern.compile("\\" + EXTENSION_DELIMITER);
    private static final Pattern REGEX_NOT_LETTER_NUMBER = Pattern.compile("[^\\w\\d]");

    @Value("${url.download.timeout}")
    private Integer downloadTimeout;

    @Value("${url.header.timeout}")
    private Integer headerTimeout;

    public Object download(String url) throws BusinessException {
        Object result = url;
        try {
            URL urlObj = new URL(url);
            if (MediaType.TEXT_HTML_VALUE.equals(getContentType(urlObj))) {
                return url;
            } else if (ResourceUtils.URL_PROTOCOL_FILE.equals(urlObj.getProtocol())) {
                throw new BusinessException("Protocol not supported");
            } else {
                File download = null;
                try {
                    download = download(urlObj);
                } catch (Exception e) {
                    // Just treat it as an url if download failed for
                    // any reason
                }

                if (download != null) {
                    if (shouldKeepDownload(download)) {
                        result = download;
                    } else {
                        download.delete();
                    }
                }
            }
        } catch (MalformedURLException e) {
            throw new BusinessException(e);
        }

        return result;
    }

    @Override
    public boolean handles(String url) {
        return REGEX_URL.matcher(url).matches();
    }

    private boolean shouldKeepDownload(File download) {
        String contentType = null;
        try (InputStream is = new BufferedInputStream(new FileInputStream(download))) {
            contentType = getContentType(is);
        } catch (IOException | BusinessException e) {
            // We won't check mimetype if something failed
        }

        return !MediaType.TEXT_HTML_VALUE.equals(contentType);
    }

    private String getContentType(URL url) throws BusinessException {
        URLConnection urlConn;
        try {
            urlConn = url.openConnection();

            setConnectionTimeout(urlConn, headerTimeout);
        } catch (IOException e) {
            throw new BusinessException(e);
        }
        String contentType = urlConn.getContentType();
        return contentType == null ? null : contentType.split(";")[0];
    }

    private void setConnectionTimeout(URLConnection urlConn, Integer timeout) {
        if (timeout != null) {
            urlConn.setConnectTimeout(timeout);
            urlConn.setReadTimeout(timeout);
        }
    }

    /**
     * Donwload URL to temporary file.
     */
    public File download(URL url) throws BusinessException {
        try {
            URLConnection urlConn = url.openConnection();

            setConnectionTimeout(urlConn, downloadTimeout);

            String originalName = getFileName(url);
            String suffix = originalName != null ? EXTENSION_DELIMITER + getExtension(originalName) : ".tmp";
            File tempFile = File.createTempFile(TEMP_FILE_PREFIX, suffix);

            try (InputStream is = urlConn.getInputStream(); OutputStream os = new FileOutputStream(tempFile)) {
                byte[] buffer = new byte[DOWNLOAD_BUFFER_SIZE];
                int length;

                // TODO Validate size so we don't overdownload
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
            }

            return tempFile;
        } catch (IOException e) {
            throw new BusinessException(e);
        }
    }

    public String getFileName(URL url) throws BusinessException {
        URLConnection urlConn;
        try {
            urlConn = url.openConnection();

            setConnectionTimeout(urlConn, headerTimeout);
        } catch (IOException e) {
            throw new BusinessException(e);
        }

        String filename = null;
        String raw = urlConn.getHeaderField("Content-Disposition");
        if(raw != null && raw.contains("=")) {
            filename = raw.replaceFirst("(?i)^.*filename=\"?([^\"]+)\"?.*$", "$1");
        }

        if (filename == null) {
            String[] urlParts = url.getPath().split("/");
            filename = urlParts[urlParts.length - 1];
        }

        return filename;
    }

    private String getExtension(String filename) {
        String[] nameParts = REGEX_EXTENSION_DELIMITER.split(filename);
        String[] extensionParts = REGEX_NOT_LETTER_NUMBER.split(nameParts[nameParts.length - 1]);
        return extensionParts[0];
    }

    private String getContentType(InputStream is) throws BusinessException {
        try {
            return new DefaultDetector().detect(is, new Metadata()).toString();
        } catch (IOException e) {
            throw new BusinessException(e);
        }
    }

}
