package me.neboard.newneboard.form.download;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InternalDownloader implements Downloader {
	private static final String PREFIX_ATTACHMENT = "attachment://";

	@Autowired
	private AttachmentService attachmentService;

	@Override
	public Object download(String url) throws BusinessException {
		String filename = url.split(PREFIX_ATTACHMENT)[1];
		Attachment attachment = attachmentService.findByFilename(filename);
		if (attachment == null) {
			throw new BusinessException("Attachment with file name " + filename + " not found");
		}
		return attachment;
	}

	@Override
	public boolean handles(String url) {
		return url.startsWith(PREFIX_ATTACHMENT);
	}
}
