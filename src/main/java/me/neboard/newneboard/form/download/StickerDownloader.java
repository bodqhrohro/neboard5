package me.neboard.newneboard.form.download;

import me.neboard.newneboard.abstracts.ExternalImageData;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.NotFoundException;
import me.neboard.newneboard.service.ExternalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StickerDownloader extends UrlDownloader {
    private static final String PREFIX_STICKER = "sticker://";

    @Autowired
    private ExternalService externalService;

    @Override
    public Object download(String url) throws BusinessException {
        try {
            String imageServiceUrl = externalService.getImageServiceUrl();

            String stickerName = url.split(PREFIX_STICKER)[1];

            ExternalImageData response = externalService.requestGet(imageServiceUrl + "/sticker/" + stickerName,
                    null, ExternalImageData.class);
            url = externalService.getFileServiceUrl(response.getName());
        } catch (NotFoundException nfe) {
            // Proceed normally, it will fail later
        }

        return super.download(url);
    }

    @Override
    public boolean handles(String url) {
        return url.startsWith(PREFIX_STICKER);
    }
}
