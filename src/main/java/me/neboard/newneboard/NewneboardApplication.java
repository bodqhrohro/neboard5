package me.neboard.newneboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class NewneboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewneboardApplication.class, args);
	}

}
