package me.neboard.newneboard.creation;

import me.neboard.newneboard.creation.attachment.FormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;

public interface AttachmentCreator<T extends FormAttachment> {
    Attachment getOrCreate(T source) throws BusinessException;
    boolean handles(FormAttachment source);
}
