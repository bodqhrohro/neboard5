package me.neboard.newneboard.creation;

import me.neboard.newneboard.abstracts.FileMetadata;
import me.neboard.newneboard.creation.attachment.FormAttachment;
import me.neboard.newneboard.creation.attachment.StreamableFormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;

public abstract class FromFileCreator {
    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private FileService fileService;

    protected Attachment createFromSource(String filename, FormAttachment source) throws BusinessException {
        FileMetadata metadata;
        try (InputStream is = getInputStream(source)) {
            metadata = fileService.create(is, filename);
        } catch (IOException e) {
            throw new BusinessException(e);
        }

        String newFilename = metadata.getName();
        Attachment attachment = attachmentService.findByFilename(newFilename);
        if (attachment == null) {
            String mimetype = metadata.getMimetype();

            attachment = new Attachment(newFilename, null);
            attachmentService.save(attachment);

            if (imageService.canCreateThumb(mimetype)) {
                imageService.createThumb(newFilename);
            }
        }

        return attachment;
    }

    private InputStream getInputStream(FormAttachment source) throws IOException {
        return ((StreamableFormAttachment) source).getInputStream();
    }

}
