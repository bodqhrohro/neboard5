package me.neboard.newneboard.creation.attachment;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public interface StreamableFormAttachment extends FormAttachment {
    InputStream getInputStream() throws IOException;
}
