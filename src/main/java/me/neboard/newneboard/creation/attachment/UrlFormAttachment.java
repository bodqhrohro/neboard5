package me.neboard.newneboard.creation.attachment;

public class UrlFormAttachment implements FormAttachment {
    private String url;

    public UrlFormAttachment(String url) {
        this.url = url;
    }

    @Override
    public long getSize() {
        return 0;
    }

    public String getUrl() {
        return url;
    }
}
