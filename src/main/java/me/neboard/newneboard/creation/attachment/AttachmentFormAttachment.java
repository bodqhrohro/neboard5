package me.neboard.newneboard.creation.attachment;

import me.neboard.newneboard.domain.Attachment;

public class AttachmentFormAttachment implements FormAttachment {
    private Attachment attachment;

    public AttachmentFormAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @Override
    public long getSize() {
        return 0;
    }

    public Attachment getAttachment() {
        return attachment;
    }
}
