package me.neboard.newneboard.creation.attachment;

import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MultipartFileFormAttachment implements FormAttachment, StreamableFormAttachment {
    private MultipartFile file;

    public MultipartFileFormAttachment(MultipartFile file) {
        this.file = file;
    }

    @Override
    public long getSize() {
        return file.getSize();
    }

    public MultipartFile getMultipartFile() {
        return file;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new BufferedInputStream(file.getInputStream());
    }
}
