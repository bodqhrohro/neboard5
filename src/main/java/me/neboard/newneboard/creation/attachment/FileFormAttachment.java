package me.neboard.newneboard.creation.attachment;

import java.io.*;

public class FileFormAttachment implements FormAttachment, StreamableFormAttachment {
    private File file;

    public FileFormAttachment(File file) {
        this.file = file;
    }

    @Override
    public long getSize() {
        return file.length();
    }

    public void deleteFile() {
        file.delete();
    }

    public File getFile() {
        return file;
    }

    @Override
    public InputStream getInputStream() throws FileNotFoundException {
        return new BufferedInputStream(new FileInputStream(file));
    }
}
