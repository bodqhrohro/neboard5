package me.neboard.newneboard.creation.attachment;

public interface FormAttachment {
    long getSize();
}
