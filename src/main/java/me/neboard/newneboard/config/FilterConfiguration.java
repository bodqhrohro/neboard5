package me.neboard.newneboard.config;

import me.neboard.newneboard.filter.PostAwareFilter;
import me.neboard.newneboard.rest.PostRestController;
import me.neboard.newneboard.web.AllThreadsController;
import me.neboard.newneboard.web.PostController;
import me.neboard.newneboard.web.ThreadController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfiguration {
    private static final String PATTERN_ALL = "*";

    @Autowired
    private PostAwareFilter postAwareFilter;

    @Bean
    public FilterRegistrationBean<PostAwareFilter> postAwareRegistration() {
        FilterRegistrationBean<PostAwareFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(postAwareFilter);
        registration.addUrlPatterns(AllThreadsController.URL_ALL_THREADS);
        registration.addUrlPatterns(AllThreadsController.URL_TAG + PATTERN_ALL);
        registration.addUrlPatterns(ThreadController.URL_PREFIX_THREAD + PATTERN_ALL);
        registration.addUrlPatterns(PostController.URL_FEED + PATTERN_ALL);
        registration.addUrlPatterns(PostController.URL_SEARCH + PATTERN_ALL);
        registration.addUrlPatterns(PostRestController.URL_THREAD_DIFF + PATTERN_ALL);
        return registration;
    }
}
