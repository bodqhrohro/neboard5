package me.neboard.newneboard.parser;

public interface PanelNode {
    String getName();
    String getPreview();
    boolean needsInput();
    String getInputHint();
    String getTagLeft();
    String getTagRight();
}
