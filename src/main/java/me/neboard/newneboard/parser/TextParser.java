package me.neboard.newneboard.parser;

import me.neboard.newneboard.exception.BusinessException;

import java.util.List;

public interface TextParser {
    String preparse(String method, String rawText) throws BusinessException;

    ParserResult parse(String method, String rawText) throws BusinessException;

    String getPanelText(String method) throws BusinessException;

    List<String> getMethods() throws BusinessException;
}
