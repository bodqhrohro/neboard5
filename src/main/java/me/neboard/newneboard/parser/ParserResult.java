package me.neboard.newneboard.parser;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ParserResult implements Serializable {
    private static final long serialVersionUID = 1L;

    private String text;
    private List<Long> reflinks;
    private List<ExternalPanelNode> tags;
    private List<String> methods;

    public String getText() {
        return text;
    }

    public List<Long> getReflinks() {
        return reflinks;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setReflinks(List<Long> reflinks) {
        this.reflinks = reflinks;
    }

    public List<ExternalPanelNode> getTags() {
        return tags;
    }

    public void setTags(List<ExternalPanelNode> tags) {
        this.tags = tags;
    }

    public List<String> getMethods() {
        return methods;
    }

    public void setMethods(List<String> methods) {
        this.methods = methods;
    }
}
