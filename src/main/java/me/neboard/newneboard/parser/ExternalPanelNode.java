package me.neboard.newneboard.parser;

public class ExternalPanelNode implements PanelNode {
    private String name;
    private String previewLeft;
    private String previewRight;
    private String tagLeft;
    private String tagRight;
    private boolean needsInput;
    private String inputHint;

    public String getPreviewLeft() {
        return previewLeft;
    }

    public void setPreviewLeft(String previewLeft) {
        this.previewLeft = previewLeft;
    }

    public String getPreviewRight() {
        return previewRight;
    }

    public void setPreviewRight(String previewRight) {
        this.previewRight = previewRight;
    }

    public String getTagLeft() {
        return tagLeft;
    }

    public void setTagLeft(String tagLeft) {
        this.tagLeft = tagLeft;
    }

    public String getTagRight() {
        return tagRight;
    }

    public void setTagRight(String tagRight) {
        this.tagRight = tagRight;
    }

    public boolean needsInput() {
        return needsInput;
    }

    public void setNeedsInput(boolean needsInput) {
        this.needsInput = needsInput;
    }

    @Override
    public String getInputHint() {
        return inputHint;
    }

    public void setInputHint(String inputHint) {
        this.inputHint = inputHint;
    }

    @Override
    public String getPreview() {
        return getPreviewLeft() + getName() + getPreviewRight();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
