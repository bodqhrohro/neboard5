package me.neboard.newneboard.domain.projection;

public interface PostIdAndUuid {
    Long getId();
    String getUuid();
}
