package me.neboard.newneboard.domain;

public interface IEntity {
    String DELIMITER = ", ";


    Long getId();
}
