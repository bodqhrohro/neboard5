package me.neboard.newneboard.domain;

import javax.persistence.*;

@Entity
@Table(name = "ban")
public class Ban {
    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column
    private String address;

    @Column
    private String reason;

    public Ban() {
    }

    public Ban(String address, String reason) {
        this.address = address;
        this.reason = reason;
    }

    public String getAddress() {
        return address;
    }

    public String getReason() {
        return reason;
    }
}
