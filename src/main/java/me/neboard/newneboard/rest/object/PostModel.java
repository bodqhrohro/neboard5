package me.neboard.newneboard.rest.object;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PostModel {
	private Long thread;
	private String text;
	private String tripcode;
	private String title;
	private Long id;

	// TODO Remove this when sources bot learns to send an attachment
	private List<String> urls;

	private List<PostAttachmentModel> attachments;
	private String url;
	private boolean opening;

	public Long getThread() {
		return thread;
	}

	public String getText() {
		return text;
	}

	public String getTripcode() {
		return tripcode;
	}

	public String getTitle() {
		return title;
	}

	public List<String> getUrls() {
		return urls;
	}

	public String getUrl() {
		return url;
	}

	public boolean isOpening() {
		return opening;
	}

	public List<PostAttachmentModel> getAttachments() {
		return attachments;
	}

	public void setThread(Long thread) {
		this.thread = thread;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setTripcode(String tripcode) {
		this.tripcode = tripcode;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setOpening(boolean opening) {
		this.opening = opening;
	}

	public void setAttachments(List<PostAttachmentModel> attachments) {
		this.attachments = attachments;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
