package me.neboard.newneboard.rest.object;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PostAttachmentModel implements Serializable {
    private int order;
    private String filename;
    private String url;

    public PostAttachmentModel() {
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getOrder() {
        return order;
    }

    public String getFilename() {
        return filename;
    }

    public String getUrl() {
        return url;
    }
}
