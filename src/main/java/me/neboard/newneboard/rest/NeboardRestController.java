package me.neboard.newneboard.rest;

import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.*;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(NeboardRestController.URL)
public class NeboardRestController {
    public static final String URL = "/api";

    private static final String URL_PREVIEW = "/preview";
    private static final String URL_HEALTH = "/health";
    private static final String URL_CONSISTENCY = "/consistency";
    private static final String URL_EXPORT = "/export";

    private static final String PARAM_RAW_TEXT = "raw_text";


    @Autowired
    private ParserService parserService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private PostService postService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private ExternalService externalService;

    @PostMapping(URL_PREVIEW)
    public String textPreview(@RequestParam(PARAM_RAW_TEXT) String text) throws BusinessException {
        return parserService.parse(parserService.preparse(text)).getText();
    }

    @GetMapping(value = URL_HEALTH, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> health() {
        Map<String, Object> status = new HashMap<>();

        long totalMemory = Runtime.getRuntime().totalMemory();
        long usedMemory = totalMemory - Runtime.getRuntime().freeMemory();

        status.put("totalMemory", FileUtils.byteCountToDisplaySize(totalMemory));
        status.put("usedMemory", FileUtils.byteCountToDisplaySize(usedMemory));
        status.put("maxMemory", FileUtils.byteCountToDisplaySize(Runtime.getRuntime().maxMemory()));


        Map<String, Object> services = new HashMap<>();
        services.put("parser", isServiceHealthy(externalService.getParserServiceUrl()));
        services.put("images", isServiceHealthy(externalService.getImageServiceUrl()));
        services.put("files", isServiceHealthy(externalService.getFileServiceUrl()));
        status.put("services", services);

        return status;
    }

    @GetMapping(value = URL_CONSISTENCY, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> consistency() {
        Map<String, Object> status = new HashMap<>();

        status.put("Missing Files", attachmentService.getMissingFiles());
        status.put("Attachment Count", attachmentService.getCount());
        status.put("Post Count", postService.getCount());

        return status;
    }

    @GetMapping(value = URL_EXPORT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> export() {
        Map<String, Object> status = new HashMap<>();

        status.put("attachments", attachmentService.getFileUrlList());

        return status;
    }

    private boolean isServiceHealthy(String serviceUrl) {
        try {
            return externalService.requestGet(serviceUrl + URL_HEALTH, null, Object.class) == null;
        } catch (BusinessException e) {
            return false;
        }
    }

}
