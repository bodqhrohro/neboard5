package me.neboard.newneboard.rest;

import me.neboard.newneboard.domain.manytomany.PostAttachment;
import me.neboard.newneboard.exception.PermissionException;
import me.neboard.newneboard.form.ThreadForm;
import me.neboard.newneboard.form.validation.ThreadFormValidator;
import me.neboard.newneboard.rest.object.PostAttachmentModel;
import me.neboard.newneboard.service.FormAttachmentService;
import me.neboard.newneboard.creation.attachment.FormAttachment;
import me.neboard.newneboard.service.impl.PostCreateDetails;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.domain.projection.PostIdAndUuid;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.NotFoundException;
import me.neboard.newneboard.form.PostForm;
import me.neboard.newneboard.form.validation.PostFormValidator;
import me.neboard.newneboard.rest.object.PostModel;
import me.neboard.newneboard.rest.object.StatusResponse;
import me.neboard.newneboard.rest.object.ThreadDiffRequest;
import me.neboard.newneboard.service.*;
import me.neboard.newneboard.web.AllThreadsController;
import me.neboard.newneboard.web.PostAwareController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.expression.ThymeleafEvaluationContext;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class PostRestController extends FormProcessingRestController implements PostAwareController {
    public static final String URL_THREAD_DIFF = "/api/post/diff/";

    private static final String URL_ADD_THREAD = "/api/post/";
    private static final String URL_ADD_POST = "/api/post/{openingPostId}";
    private static final String URL_GET_POST = "/api/post/{postId}";
    private static final String URL_GET_REPLIES = "/api/post/{postId}/replies";
    private static final String URL_DELETE_POST = "/api/post/{postId}/delete";
    private static final String URL_PURGE = "/api/{postId}/purge/";
    private static final String URL_SEARCH = "/api/post/";

    private static final String URL_SERVICE_ADD_POST = "/api/service/add-post";

    private static final String HEADER_API_KEY = "X-API-SECRET";

    private static final String ATTR_UPDATED_POSTS = "updated";
    private static final String ATTR_DELETED_POSTS = "deleted";
    private static final String ATTR_THREAD_FULLNESS = "threadFullness";
    private static final String ATTR_THREAD_STATUS = "threadStatus";

    private static final String STATUS_OK = "ok";
    private static final String STATUS_ERROR = "error";

    @Value("#{'${api.secrets}'.split(',')}")
    private List<String> apiSecrets;

    @Value("${api.posts.per.page}")
    private int postsPerPage;

    @Autowired
    @Qualifier("postFormValidator")
    private PostFormValidator postValidator;

    @Autowired
    private ThreadFormValidator threadValidator;

    @Autowired
    private PostService postService;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private ThymeleafEvaluationContext thymeleafContext;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private TripcodeService tripcodeService;

    @Autowired
    private FormAttachmentService formAttachmentService;

    @Autowired
    private BanService banService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        Object target = binder.getTarget();
        if (target != null) {
            Validator validator = null;
            if (target instanceof ThreadForm) {
                validator = threadValidator;
            } else if (target instanceof PostForm) {
                validator = postValidator;
            }

            if (validator != null) {
                binder.setValidator(validator);
            }
        }
    }

    @PostMapping(value = URL_SERVICE_ADD_POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StatusResponse> serviceAddPost(@RequestBody PostModel postModel, HttpServletRequest request) throws BusinessException {
        String apiKey = request.getHeader(HEADER_API_KEY);
        
        boolean valid = apiSecrets.contains(apiKey);

        ResponseEntity<StatusResponse> entity;
        StatusResponse response = new StatusResponse();
        if (!valid) {
            entity = ResponseEntity.badRequest().body(response);
            response.addError("Invalid API key");
        } else {
            entity = ResponseEntity.ok(response);
        }

        PostThread thread = postService.findById(postModel.getThread()).getThread();

        List<FormAttachment> urls = new ArrayList<>();
        if (postModel.getUrls() != null) {
            urls = postModel.getUrls().stream()
                    .map(formAttachmentService::getAttachment)
                    .collect(Collectors.toList());
        }

        List<Attachment> attachments = attachmentService.createFromFormData(urls);

        try {
            PostCreateDetails details = new PostCreateDetails(postModel.getTitle(),
                    postModel.getText(), attachments, thread, null,
                    tripcodeService.encodeTripcode(postModel.getTripcode()));
            postService.create(details);

            thread = threadService.findById(thread.getId());
            response.addData(ATTR_THREAD_STATUS, thread.getStatus());
        } catch (BusinessException e) {
            response.addError(e.getMessage());
            entity = ResponseEntity.badRequest().body(response);
        }

        return entity;
    }

    @PostMapping(value = URL_ADD_POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StatusResponse> addPost(@PathVariable Long openingPostId, @Valid PostForm form,
            BindingResult result, HttpServletRequest request) throws BusinessException {
        boolean valid = !result.hasErrors();

        ResponseEntity<StatusResponse> entity;
        StatusResponse response = new StatusResponse();
        if (!valid) {
            saveErrorMessages(result, response);
            entity = ResponseEntity.badRequest().body(response);
        } else {
            entity = ResponseEntity.ok(response);
            PostThread thread = postService.findById(openingPostId).getThread();

            try {
                PostCreateDetails details = postService.getCreateDetailsBuilder()
                        .address(utilService.getIpFromRequest(request))
                        .form(form)
                        .thread(thread)
                        .build();
                postService.create(details);
            } catch (BusinessException e) {
                response.addError(e.getMessage());
                entity = ResponseEntity.badRequest().body(response);
            }
        }

        return entity;
    }

    @PostMapping(value = URL_THREAD_DIFF, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> getThreadDiff(@RequestBody ThreadDiffRequest request) {
        PostThread thread = postService.findById(request.getThreadId()).getThread();

        List<String> deletedPostIds = new ArrayList<>(request.getUuids());
        List<PostIdAndUuid> posts = postService.findByThread(thread, PostIdAndUuid.class);
        List<Long> updatedPostIds = new ArrayList<>();
        for (PostIdAndUuid post : posts) {
            String uuid = post.getUuid();
            if (deletedPostIds.contains(uuid)) {
                deletedPostIds.remove(uuid);
            } else {
                updatedPostIds.add(post.getId());
            }
        }

        List<Post> updatedPosts = postService.findByThreadAndIdIn(thread, updatedPostIds, Post.class);

        List<String> postContents = new ArrayList<>();
        if (!posts.isEmpty()) {
            for (Post post : updatedPosts) {
                Context context = new Context(LocaleContextHolder.getLocale());

                context.setVariable(ATTR_POST, post);
                context.setVariable(ATTR_THREAD, thread);
                context.setVariable(ATTR_PREVIEW, false);
                context.setVariable(ThymeleafEvaluationContext.THYMELEAF_EVALUATION_CONTEXT_CONTEXT_VARIABLE_NAME,
                        thymeleafContext);

                // TODO Return post models and let the client render them
                postContents.add(templateEngine.process(PostAwareController.TEMPLATE_POST, context));
            }
        }

        Map<String, Object> result = new HashMap<>();
        result.put(ATTR_UPDATED_POSTS, postContents);
        result.put(ATTR_DELETED_POSTS, deletedPostIds);
        result.put(ATTR_THREAD_FULLNESS, threadService.getThreadFullness(thread));
        result.put(ATTR_THREAD_STATUS, thread.getStatus());

        return result;
    }

    @GetMapping(value = URL_GET_POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PostModel> getPostUrl(@PathVariable Long postId, HttpServletRequest request) throws BusinessException {
        ResponseEntity<PostModel> entity;
        Post post = postService.findById(postId);
        if (post != null) {
            entity = ResponseEntity.ok(toModel(post));
        } else {
            entity = ResponseEntity.notFound().build();
        }

        return entity;
    }

    @PostMapping(value = URL_PURGE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> purgeUser(@PathVariable Long postId) throws PermissionException {
        settingsService.validatePermission();

        Post post = postService.findById(postId);
        postService.purgeFromPost(post);

        banService.banUser(post.getPosterAddress(), "Post #" + post.getId());

        postService.purgeFromPost(post);

        return ResponseEntity.noContent().build();
    }

    @PostMapping(URL_DELETE_POST)
    public ResponseEntity<StatusResponse> deletePost(@PathVariable Long postId) throws BusinessException {
        settingsService.validatePermission();

        StatusResponse response;

        Post post = postService.findById(postId);
        if (post == null) {
            throw new NotFoundException("Post not found");
        }

        if (post.isOpening()) {
            threadService.delete(post.getThread());
            response = new StatusResponse(AllThreadsController.URL_ALL_THREADS);
        } else {
            response = new StatusResponse();
            postService.delete(post);
        }

        return ResponseEntity.ok(response);
    }

    @PostMapping(value = URL_ADD_THREAD, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StatusResponse> createThread(@Valid ThreadForm form, BindingResult result, HttpServletRequest request) throws BusinessException {
        boolean valid = !result.hasErrors();

        ResponseEntity<StatusResponse> entity;
        StatusResponse response = new StatusResponse();
        if (!valid) {
            saveErrorMessages(result, response);

            entity = ResponseEntity.badRequest().body(response);
        } else {
            PostCreateDetails details = postService.getCreateDetailsBuilder()
                    .address(utilService.getIpFromRequest(request))
                    .form(form)
                    .build();

            Post post = postService.create(details);
            response.setUrl(postService.getUrl(post));

            entity = ResponseEntity.ok(response);
        }

        return entity;
    }

    private PostModel toModel(Post post) {
        PostModel model = new PostModel();

        model.setOpening(post.isOpening());
        model.setUrl(postService.getUrl(post));
        model.setTitle(post.getTitle());
        model.setText(post.getText());
        model.setId(post.getId());

        if (!post.isOpening()) {
            model.setThread(post.getThread().getOpeningPost().getId());
        }

        List<PostAttachmentModel> attachmentModels = new ArrayList<>();
        for (PostAttachment postAttachment : post.getPostAttachments()) {
            PostAttachmentModel pam = new PostAttachmentModel();

            pam.setOrder(postAttachment.getOrder());
            Attachment attachment = postAttachment.getAttachment();
            if (attachment.isFileAttachment()) {
                pam.setFilename(attachment.getFilename());
            } else {
                pam.setUrl(attachment.getUrl());
            }
            attachmentModels.add(pam);
        }
        model.setAttachments(attachmentModels);

        return model;
    }

    @GetMapping(value = URL_SEARCH)
    public ResponseEntity<List<PostModel>> search(@RequestParam String query,
                                                  @RequestParam(required = false) Boolean threadsOnly,
                                                  @RequestParam Optional<Integer> page) {
        Pageable byPubTime = utilService.getPageable(page,
                postsPerPage, Sort.by(Sort.Order.desc(Post.PUB_TIME)));

        // TODO Search only OPs

        List<Post> posts = postService.search(byPubTime, query).getContent();
        List<PostModel> models = posts.stream().map(this::toModel).collect(Collectors.toList());
        return ResponseEntity.ok(models);
    }

}
