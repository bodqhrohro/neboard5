package me.neboard.newneboard.rest;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.exception.PermissionException;
import me.neboard.newneboard.service.BanService;
import me.neboard.newneboard.service.PostService;
import me.neboard.newneboard.service.SettingsService;
import me.neboard.newneboard.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AdminRestController.URL)
public class AdminRestController {
    public static final String URL = "/api/admin";

    private static final String URL_BAN = "/ban";

    @Autowired
    private PostService postService;

    @Autowired
    private BanService banService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private ThreadService threadService;

    @PostMapping(URL_BAN)
    public ResponseEntity<Void> banUser(@RequestParam String address, @RequestParam long postId) throws PermissionException {
        settingsService.validatePermission();

        Post post = postService.findById(postId);
        banService.banUser(address, "Post #" + post.getId());

        return ResponseEntity.noContent().build();
    }

}
