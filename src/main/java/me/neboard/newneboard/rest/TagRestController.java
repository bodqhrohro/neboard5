package me.neboard.newneboard.rest;

import me.neboard.newneboard.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@RestController
public class TagRestController {
	private static final String URL_TAGS = "/api/tags";

	@Autowired
	private TagService tagService;

	@GetMapping(URL_TAGS)
	public Set<String> filteredTags(@RequestParam String term) {
		SortedSet<String> tagNames = new TreeSet<>();

		tagService.findStartingWith(term)
				.forEach(tag -> tagNames.add(tag.getName()));

		return tagNames;
	}
}
