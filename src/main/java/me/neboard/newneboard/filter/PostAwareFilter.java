package me.neboard.newneboard.filter;

import me.neboard.newneboard.domain.User;
import me.neboard.newneboard.service.SettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

@Component
public class PostAwareFilter implements Filter {
    private static final String ATTR_USER = "user";

    private static final String ATTR_TIMEZONE = "timezone";
    private static final String ATTR_ADMIN = "admin";
    private static final String ATTR_THEME = "theme";

    @Autowired
    private SettingsService settingsService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        User user = settingsService.getOrCreateUser();

        request.setAttribute(ATTR_USER, user);

        request.setAttribute(ATTR_TIMEZONE, settingsService.getTimezone(user));
        request.setAttribute(ATTR_ADMIN, settingsService.isAdmin(user));
        request.setAttribute(ATTR_THEME, settingsService.getTheme(user));

        chain.doFilter(request, response);
    }

}
