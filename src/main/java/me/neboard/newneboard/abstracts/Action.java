package me.neboard.newneboard.abstracts;

import me.neboard.newneboard.exception.BusinessException;

@FunctionalInterface
public interface Action<T> {
	T execute() throws BusinessException;
}