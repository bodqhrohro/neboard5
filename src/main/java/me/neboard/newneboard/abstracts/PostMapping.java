package me.neboard.newneboard.abstracts;

import java.io.Serializable;

public class PostMapping implements Serializable {
    private static final long serialVersionUID = 1L;

    private String url;
    private boolean opening;

    public PostMapping(String url, boolean opening) {
        this.url = url;
        this.opening = opening;
    }

    public String getUrl() {
        return url;
    }

    public boolean isOpening() {
        return opening;
    }
}
