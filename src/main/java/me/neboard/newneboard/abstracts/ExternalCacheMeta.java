package me.neboard.newneboard.abstracts;

public class ExternalCacheMeta {
    private String cacheName;
    private String cacheUpdateUrl;
    private String cacheUpdateTime;

    public ExternalCacheMeta(String cacheName, String cacheUpdateUrl) {
        this.cacheName = cacheName;
        this.cacheUpdateUrl = cacheUpdateUrl;
    }

    public String getCacheName() {
        return cacheName;
    }

    public String getCacheUpdateUrl() {
        return cacheUpdateUrl;
    }

    public String getCacheUpdateTime() {
        return cacheUpdateTime;
    }

    public void setCacheUpdateTime(String cacheUpdateTime) {
        this.cacheUpdateTime = cacheUpdateTime;
    }
}
