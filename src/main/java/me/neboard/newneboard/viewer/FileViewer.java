package me.neboard.newneboard.viewer;

import me.neboard.newneboard.abstracts.Dimensions;
import me.neboard.newneboard.abstracts.ExternalImageData;
import me.neboard.newneboard.abstracts.ImageFileMetadata;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.ExternalService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.ImageService;
import me.neboard.newneboard.web.ExternalImageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class FileViewer extends AbstractViewer {
    protected static final String HTML_IMAGE = "<img class='url-image' src='${url}' width='${width}' height='${height}'>";

    private static final String FORMAT_FILE_NAME = "/images/file.png";

    @Autowired
    private FileService fileService;

    @Autowired
    private ExternalService externalService;

    @Autowired
    private ImageService imageService;

    @Override
    public String getFormatView(Attachment attachment) throws BusinessException {
        return HTML_IMAGE;
    }

    @Override
    public boolean handles(Attachment attachment) throws BusinessException {
        return false;
    }

    protected String getGenericImage() throws BusinessException {
        return FORMAT_FILE_NAME;
    }

    @Override
    public Map<String, String> getData(Attachment attachment) throws BusinessException {
        Map<String, String> data = new HashMap<>();

        ExternalImageData imageData = null;

        if (attachment.isFileAttachment()) {
            String mimetype = fileService.getMimetype(attachment.getFilename());
            if (mimetype == null) {
                data = MissingFileViewer.getMissingFileData(imageService, attachment);
            } else {
                imageData = externalService.getFileImage(mimetype);
            }
        }

        String imageName;
        long width;
        long height;

        if (data.isEmpty()) {
            if (imageData == null) {
                String genericImageName = getGenericImage();
                Dimensions imageSize = imageService.getStaticImageSizeFromClasspath(genericImageName);

                imageName = genericImageName;
                width = imageSize.getWidth();
                height = imageSize.getHeight();
            } else {
                ImageFileMetadata metadata = fileService.getMetadata(imageData.getName(), ImageFileMetadata.class);

                imageName = ExternalImageController.URL_FILES + imageData.getName();
                width = metadata.getWidth();
                height = metadata.getHeight();
            }

            data.put(DATA_URL, imageName);
            data.put(DATA_WIDTH, String.valueOf(width));
            data.put(DATA_HEIGHT, String.valueOf(height));
        }

        return data;
    }

}
