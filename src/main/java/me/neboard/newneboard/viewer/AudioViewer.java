package me.neboard.newneboard.viewer;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class AudioViewer extends FileViewer {
    private static final String HTML_AUDIO = "<audio controls preload='metadata' src='${url}'></audio>";

    private static final Set<String> AUDIO_MIMETYPES = new HashSet<>() {{
        add("audio/ogg");
        add("audio/vorbis");
        add("application/ogg");
        add("audio/x-oggflac");
        add("audio/x-flac");
        add("audio/mpeg");
    }};

    @Autowired
    private UtilService utilService;

    @Autowired
    private FileService fileService;

    @Override
    public boolean handles(Attachment attachment) throws BusinessException {
        return attachment.isFileAttachment() && isAudio(
                fileService.getMimetype(attachment.getFilename()));
    }

    @Override
    public String getFormatView(Attachment attachment) throws BusinessException {
        return HTML_AUDIO;
    }

    private boolean isAudio(String mimetype) {
        return AUDIO_MIMETYPES.contains(mimetype);
    }
}
