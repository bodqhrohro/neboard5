package me.neboard.newneboard.viewer;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.FileService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractViewer implements AttachmentViewer {
    private static final String HTML_URL = "<div class='image'><a %s href='%s'>%s</a><div class='image-metadata'>%s</div></div>";
    private static final String HTML_METADATA = "<a href='%s' download=''>%s, %s</a> <a href='/feed/?filename=%s'>&#10697;</a>";

    private static final Pattern REGEX_DATA_ITEM = Pattern.compile("\\$\\{(\\w+)\\}");

    @Autowired
    private FileService fileService;

    @Autowired
    private AttachmentService attachmentService;

    @Override
    public String getView(Attachment attachment) throws BusinessException {
        return String.format(HTML_URL, getLinkAttributes(), getFileUrl(attachment), getFormatView(attachment), getMetadata(attachment));
    }

    protected String getMetadata(Attachment attachment) throws BusinessException {
        String mimetype = fileService.getMimetype(attachment.getFilename());
        if (mimetype == null) {
            mimetype = "unknown";
        }
        return String.format(HTML_METADATA,
                attachmentService.getUrl(attachment),
                mimetype,
                FileUtils.byteCountToDisplaySize(fileService.getFileSize(attachment.getFilename())),
                attachment.getFilename());
    }

    protected abstract String getFormatView(Attachment attachment) throws BusinessException;

    protected String getLinkAttributes() {
        return "";
    }

    private String getFileUrl(Attachment attachment) {
        return attachmentService.getUrl(attachment);
    }

    protected Map<String, String> getData(Attachment attachment) throws BusinessException {
        return new HashMap<>();
    }

    @Override
    public String replaceData(final String result, Attachment attachment) throws BusinessException {
        return replaceData(result, getData(attachment));
    }

    @Override
    public String replaceData(final String result, Map<String, String> data) {
        Matcher matcher = REGEX_DATA_ITEM.matcher(result);
        StringBuilder sb = new StringBuilder();
        while (matcher.find()) {
            matcher.appendReplacement(sb, data.get(matcher.group(1)));
        }
        matcher.appendTail(sb);

        return sb.toString();
    }
}
