package me.neboard.newneboard.viewer;

import me.neboard.newneboard.abstracts.ImageFileMetadata;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.ImageService;
import me.neboard.newneboard.service.UtilService;
import me.neboard.newneboard.web.ExternalImageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ImageViewer extends AbstractViewer {
    private static final String HTML_IMAGE = "<img class='post-image-preview' src='${thumbUrl}'"
        + " alt='${id}' height='${thumbHeight}' width='${thumbWidth}' data-width='${width}' data-height='${height}' data-orientation='${orientation}' />";
    private static final String ATTR_CLASS_THUMB = "class='thumb'";

    @Autowired
    private UtilService utilService;

    @Autowired
    private FileService fileService;

    @Autowired
    private ImageService imageService;

    @Override
    public boolean handles(Attachment attachment) throws BusinessException {
        return attachment.isFileAttachment() && utilService.isImage(
                fileService.getMimetype(attachment.getFilename()));
    }

    @Override
    public String getFormatView(Attachment attachment) throws BusinessException {
        return HTML_IMAGE;
    }

    @Override
    protected Map<String, String> getData(Attachment attachment) throws BusinessException {
        Map<String, String> data = super.getData(attachment);

        String filename = attachment.getFilename();
        String thumbName = imageService.getThumbName(filename);

        String result;
        try {
            if (!fileService.fileExists(thumbName)) {
                // Regenerate thumb if it was lost for some reason
                imageService.createThumb(filename);
            }

            ImageFileMetadata metadataThumb = fileService.getMetadata(thumbName, ImageFileMetadata.class);
            ImageFileMetadata metadataFull = fileService.getMetadata(filename, ImageFileMetadata.class);

            int exifOrientation = metadataFull.getOrientation();

            data.put("thumbUrl", ExternalImageController.URL_FILES + thumbName);
            data.put("thumbWidth", String.valueOf(metadataThumb.getWidth()));
            data.put("thumbHeight", String.valueOf(metadataThumb.getHeight()));
            data.put(DATA_WIDTH, String.valueOf(metadataFull.getWidth()));
            data.put(DATA_HEIGHT, String.valueOf(metadataFull.getHeight()));
            data.put("id", String.valueOf(attachment.getId()));
            data.put("orientation", String.valueOf(exifOrientation));
        } catch (BusinessException e) {
            data.putAll(MissingFileViewer.getMissingFileData(imageService, attachment));
        }

        return data;
    }

    @Override
    protected String getLinkAttributes() {
        return ATTR_CLASS_THUMB;
    }
}
