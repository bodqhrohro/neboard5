package me.neboard.newneboard.viewer;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;

import java.util.Map;

public interface AttachmentViewer {
    String DATA_URL = "url";
    String DATA_WIDTH = "width";
    String DATA_HEIGHT = "height";

    String getView(Attachment attachment) throws BusinessException;

    boolean handles(Attachment attachment) throws BusinessException;

    String replaceData(String result, Attachment attachment) throws BusinessException;

    /**
     * Replaces ${param} in the cached view result with named parameters from
     * the data map. Used to apply the varying data and cache the static one.
     */
    String replaceData(String result, Map<String, String> data);
}
