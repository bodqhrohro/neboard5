package me.neboard.newneboard.viewer;

import me.neboard.newneboard.abstracts.Dimensions;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.ImageService;
import me.neboard.newneboard.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MissingFileViewer extends FileViewer {

	private static final String MISSING_FILE_FORMAT = "/images/missing.png";
	private static final String KEY_LOST_FILE = "attachment.lost";

	@Autowired
	private FileService fileService;

	@Autowired
	private UtilService utilService;

	@Autowired
	private ImageService imageService;

	@Override
	public boolean handles(Attachment attachment) {
		return attachment.isFileAttachment() && !fileService.fileExists(attachment.getFilename());
	}

	public static String getMissingFormatView(ImageService imageService, Attachment attachment) throws BusinessException {
		Dimensions imageSize = imageService.getStaticImageSizeFromClasspath(MISSING_FILE_FORMAT);
		return String.format(HTML_IMAGE, MISSING_FILE_FORMAT, imageSize.getWidth(), imageSize.getHeight());
	}

	@Override
	protected String getMetadata(Attachment attachment) throws BusinessException {
		return utilService.getMessage(KEY_LOST_FILE);
	}

	public static Map<String, String> getMissingFileData(ImageService imageService, Attachment attachment) throws BusinessException {
		Map<String, String> data = new HashMap<>();

		Dimensions imageSize = imageService.getStaticImageSizeFromClasspath(MISSING_FILE_FORMAT);

		data.put(DATA_URL, MISSING_FILE_FORMAT);
		data.put(DATA_WIDTH, String.valueOf(imageSize.getWidth()));
		data.put(DATA_HEIGHT, String.valueOf(imageSize.getHeight()));

		return data;
	}
}
