package me.neboard.newneboard.viewer;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.web.ExternalImageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
public class VideoViewer extends AbstractViewer {
    private static final String HTML_VIDEO = "<video width='200' height='150' controls preload='metadata' src='${url}'></video>";

    private static final String MIMETYPE_VIDEO = "video/";

    private static final Set<String> VIDEO_MIMETYPES = new HashSet<>() {{
        add("video/mp4");
        add("application/x-matroska");
        add("video/quicktime");
    }};

    @Autowired
    private FileService fileService;

    @Autowired
    private AttachmentService attachmentService;

    @Override
    public boolean handles(Attachment attachment) throws BusinessException {
        return attachment.isFileAttachment() && isVideo(
                fileService.getMimetype(attachment.getFilename()));
    }

    @Override
    public String getFormatView(Attachment attachment) {
        return HTML_VIDEO;
    }

    private boolean isVideo(String mimetype) {
        return VIDEO_MIMETYPES.contains(mimetype) || (mimetype != null && mimetype.startsWith(MIMETYPE_VIDEO));
    }

    @Override
    protected Map<String, String> getData(Attachment attachment) throws BusinessException {
        Map<String, String> data = super.getData(attachment);
        data.put(DATA_URL, ExternalImageController.URL_FILES + attachment.getFilename());
        return data;
    }
}
