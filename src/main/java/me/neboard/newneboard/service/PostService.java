package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.*;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.form.CreatableForm;
import me.neboard.newneboard.service.impl.PostCreateDetails;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

public interface PostService extends SearchService<Post>, LinkAwareService<Post>,
        DeleteAwareService<Post> {
    Post create(PostCreateDetails details) throws BusinessException;

    List<Post> findByPosterAddress(String address);

    double getPostSpeed();

    int getCountByThread(PostThread thread);

    Post findById(long id);
    <T> List<T> findByThreadAndIdIn(PostThread thread, List<Long> ids, Class<T> type);
    <T> List<T> findByThread(PostThread thread, Class<T> type);
    Page<Post> findByAttachmentFilename(Pageable pageable, String filename);
    Page<Post> findByExample(Pageable pageable, Example<Post> example);

    String getReplyLinks(Post post);

    List<Post> findTodayActiveThreads(Tag tag);

    Post updatePost(long postId, CreatableForm form) throws BusinessException;

    void purgeFromPost(Post post);

    void purgePosts(List<Post> posts, boolean ban);

    long getCount();

    Collection<Post> getThreadLastPosts(PostThread thread);

    List<Attachment> getAttachments(Post post);

    PostCreateDetails.PostCreateDetailsBuilder getCreateDetailsBuilder();

    String getShortTitle(Post post);
}
