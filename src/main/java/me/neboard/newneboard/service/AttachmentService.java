package me.neboard.newneboard.service;

import me.neboard.newneboard.creation.attachment.FormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.exception.BusinessException;

import java.util.List;
import java.util.Set;

public interface AttachmentService {
    String CACHE_ATTACH_COUNT_BY_THREAD = "attachCountByThread";
    String CACHE_VIEW_TEMPLATE = "attachViewTemplate";

    List<Attachment> createFromFormData(List<FormAttachment> formAttachments) throws BusinessException;

    String getView(Attachment attachment) throws BusinessException;

    long getCountByThread(PostThread thread);

    Set<String> getMissingFiles();

    List<String> getFileUrlList();

    long getCount();

    Attachment findByFilename(String filename);
    Attachment findByUrl(String url);
    List<Attachment> findByThread(PostThread thread);

    Attachment save(Attachment attachment);

    String getUrl(Attachment attachment);
}
