package me.neboard.newneboard.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SearchService<T> {
    Page<T> search(Pageable pageable, String query);

    default List<T> search(String query) {
        Pageable pageable = Pageable.unpaged();
        return search(pageable, query).getContent();
    }
}
