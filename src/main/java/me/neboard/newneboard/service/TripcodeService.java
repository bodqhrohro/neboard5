package me.neboard.newneboard.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class TripcodeService {
    private static final String HTML_TRIPCODE = "<a class='tripcode' title='%s' href='/feed/?tripcode=%s' style='border: solid 2px #%s; border-left: solid 1ex #%s;'>%s</a>";
    private static final int COLOR_LENGTH = 6;

    public String encodeTripcode(String tripcodeSource) {
        return StringUtils.isNotBlank(tripcodeSource)
                ? DigestUtils.md5Hex(tripcodeSource).toUpperCase()
                : null;
    }

    public String formatTripcode(String tripcode) {
        String result;
        if (tripcode == null || tripcode.length() < COLOR_LENGTH) {
            result = null;
        } else {
            String color = tripcode.substring(0, COLOR_LENGTH);
            result = String.format(HTML_TRIPCODE, tripcode, tripcode, color, color, color);
        }
        return result;
    }
}
