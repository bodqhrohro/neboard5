package me.neboard.newneboard.service;

import me.neboard.newneboard.abstracts.FileMetadata;
import me.neboard.newneboard.exception.BusinessException;

import java.io.InputStream;
import java.util.Map;

public interface FileService {
    boolean fileExists(String filename);

    <T> T getMetadata(String filename, Class<T> clazz) throws BusinessException;

    long getFileSize(String filename) throws BusinessException;

    String getMimetype(String filename) throws BusinessException;

    void delete(String filename) throws BusinessException;

    FileMetadata create(InputStream is, String originalFilename) throws BusinessException;
    FileMetadata create(Map<String, Object> options) throws BusinessException;
}
