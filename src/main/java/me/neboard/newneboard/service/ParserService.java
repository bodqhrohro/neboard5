package me.neboard.newneboard.service;

import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.parser.ParserResult;

import java.util.List;

public interface ParserService {
    String preparse(String rawText) throws BusinessException;

    ParserResult parse(String rawText) throws BusinessException;

    String getPanelText() throws BusinessException;

    List<String> getMethods() throws BusinessException;

    int getLineBreaks();
}
