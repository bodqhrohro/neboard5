package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.Tag;

import java.util.List;
import java.util.Set;

public interface TagService extends SearchService<Tag>, LinkAwareService<Tag> {
    Tag findByName(String name);

    Tag createOrUpdate(Tag tag);

    Tag createOrUpdate(String name);

    List<Tag> findStartingWith(String name);

    Set<Tag> getTagsFromNames(String[] tagNames);
}
