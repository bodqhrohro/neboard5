package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.viewer.AttachmentViewer;

public interface ViewService {
    AttachmentViewer getViewer(Attachment attachment);
}
