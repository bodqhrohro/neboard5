package me.neboard.newneboard.service;

import me.neboard.newneboard.creation.AttachmentCreator;
import me.neboard.newneboard.creation.attachment.FormAttachment;

public interface FormAttachmentService {
    <T extends FormAttachment> AttachmentCreator<T> getCreator(T source);
    FormAttachment getAttachment(Object source);
}
