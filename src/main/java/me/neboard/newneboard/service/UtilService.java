package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.IEntity;
import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class UtilService {
    private static final String COLOR_DEFAULT = "000";
    private static final int COLOR_LENGTH = 6;
    private static final int PAGINATOR_LOOKAROUND = 3;

    private static final String CACHE_COLORED_HASH = "coloredHash";
    private static final String CACHE_PAGINATOR_BAR = "paginatorBar";

    private static final Set<String> IMAGE_MIMETYPES = new HashSet<>() {{
        add(MediaType.IMAGE_JPEG_VALUE);
        add(MediaType.IMAGE_PNG_VALUE);
        add(MediaType.IMAGE_GIF_VALUE);
        add("image/webp");
        add("image/bmp");
    }};

    private static final int DEFAULT_PAGE = 0;

    private static final String HTML_PAGE_LINK = "<div class='page_link'><a href='%s?page=%d%s'>%s</a></div>";

    private static final String TIMEZONE_NAME = "UTC%+d: %s";

    private static final String DELIMITER_URL_PARAM = "&";
    private static final String DELIMITER_HEADER = ",";

    private static final String MSG_PAGE_PREV = "page.prev";
    private static final String MSG_PAGE_NEXT = "page.next";

    private static final String HEADER_X_FORWARDED_FOR = "X-Forwarded-For";

    private static final Pattern REGEX_QUERY_STR_PAGE = Pattern.compile("page=\\d+&?");

    private static final String PAGINATOR_BAR_ITEM = "<a href='%s?page=%d%s'>%d</a>";
    private static final String PAGINATOR_BAR_ITEM_CURRENT = "<a href='%s?page=%d%s' class='current_page'>%d</a>";
    private static final String PAGINATOR_DELIMITER = " ";

    private static final String HEADER_CACHE_CONTROL = "cache-control";
    private static final String CACHE_NO_CACHE = "no-cache, no-store, must-revalidate";

    @Value("${site.name}")
    private String siteName;

    @Value("${site.version}")
    private String siteVersion;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private TagService tagService;

    public String formatDate(Date date, TimeZone timeZone) {
        LocalDateTime localDate = LocalDateTime.ofInstant(date.toInstant(), ZoneId.of(timeZone.getID()));
        DateFormat sdf = SimpleDateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, LocaleContextHolder.getLocale());
        return sdf.format(Date.from(localDate.atZone(ZoneId.systemDefault()).toInstant()));
    }

    public String getSiteName() {
        return siteName;
    }

    public String getSiteVersion() {
        return siteVersion;
    }

    /**
     * Gets pageable object required for paged requests and output of some pages.
     */
    public Pageable getPageable(Optional<Integer> page, int perPage) {
        return PageRequest.of(page.orElse(DEFAULT_PAGE), perPage);
    }

    public Pageable getPageable(Optional<Integer> page, int perPage, Sort sort) {
        return PageRequest.of(page.orElse(DEFAULT_PAGE), perPage, sort);
    }

    /**
     * Gets unpaged object that is used to get all results.
     */
    public Pageable getPageable() {
        return Pageable.unpaged();
    }

    public boolean isImage(String mimetype) {
        return IMAGE_MIMETYPES.contains(mimetype);
    }

    public String getMessage(String key, Object... arguments) {
        try {
            return messageSource.getMessage(key, arguments, LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e) {
            return messageSource.getMessage(key, arguments, Locale.getDefault());
        }
    }

    public String getPrevPageLink(Page page, HttpServletRequest request) {
        if (page.hasPrevious()) {
            return getPageLink(page.getNumber() - 1,
                    request.getRequestURI(), request.getQueryString(),
                    MSG_PAGE_PREV);
        } else {
            return "";
        }
    }

    public String getNextPageLink(Page page, HttpServletRequest request) {
        if (page.hasNext()) {
            return getPageLink(page.getNumber() + 1,
                    request.getRequestURI(), request.getQueryString(),
                    MSG_PAGE_NEXT);
        } else {
            return "";
        }
    }

    public String getPaginatorBar(Page page, HttpServletRequest request) throws BusinessException {
        PageInfo pageInfo = new PageInfo(page);
        // FIXME Cache should include request uri and requests parameters,
        // or form some type of a template that can easily be replaced
//        int pagesCount = pageInfo.getPagesCount();
        int pageNumber = pageInfo.getPageNumber();

//        String cacheKey = cacheService.getKey(pageNumber, pagesCount);

        return getBarPageNumbers(pageInfo)
                        .stream()
                        .map(number -> {
                            String template = number.equals(pageNumber)
                                    ? PAGINATOR_BAR_ITEM_CURRENT : PAGINATOR_BAR_ITEM;
                            return String.format(template,
                                    request.getRequestURI(),
                                    number,
                                    replacePageQueryString(request.getQueryString()),
                                    number);
                        })
                        .collect(Collectors.joining(PAGINATOR_DELIMITER));
    }

    private String getPageLink(int newPageNumber, String currentPageUrl, String queryString, String msg) {
        queryString = replacePageQueryString(queryString);
        return String.format(HTML_PAGE_LINK, currentPageUrl, newPageNumber,
                queryString, getMessage(msg));
    }

    private String replacePageQueryString(String queryString) {
        return StringUtils.isEmpty(queryString) ? ""
                : DELIMITER_URL_PARAM + REGEX_QUERY_STR_PAGE.matcher(queryString).replaceAll("");
    }

    public String getIpFromRequest(HttpServletRequest request) {
        String address;

        String xHeader = request.getHeader(HEADER_X_FORWARDED_FOR);
        if (xHeader == null) {
            address = request.getRemoteAddr();
        } else {
            address = xHeader.split(DELIMITER_HEADER)[0];
        }

        return address;
    }

    public String getTimeZoneName(TimeZone tz) {
        return String.format(TIMEZONE_NAME, TimeUnit.MILLISECONDS.toHours(tz.getRawOffset()), tz.getID());
    }

    public Map<String, String> getMessages(Set<String> keys) {
        return keys.stream().collect(Collectors.toMap(key -> key, this::getMessage));
    }

    public void noCacheResponse(HttpServletResponse response) {
        response.addHeader(HEADER_CACHE_CONTROL, CACHE_NO_CACHE);
    }

    public String getUrlList(Collection<Tag> objects) {
        return getUrlList(objects, false);
    }

    public String getUrlList(Collection<Tag> objects, boolean trailingDelimiter) {
        String result = objects.stream()
                .map(tagService::getHtmlLink)
                .collect(Collectors.joining(IEntity.DELIMITER));
        if (trailingDelimiter && !result.isEmpty()) {
            result += IEntity.DELIMITER;
        }
        return result;
    }

    public String getColoredHash(String str) throws BusinessException {
        String color;
        if (!StringUtils.isEmpty(str)) {
            color = cacheService.get(CACHE_COLORED_HASH, str, () ->
                    DatatypeConverter.printHexBinary(
                    DigestUtils.md5Digest(str.getBytes()))
                    .substring(0, COLOR_LENGTH), String.class);
        } else {
            color = COLOR_DEFAULT;
        }
        return color;
    }

    private Collection<Integer> getBarPageNumbers(PageInfo pageInfo) {
        int pagesCount = pageInfo.getPagesCount();
        int pageNumber = pageInfo.getPageNumber();

        int leftLimit = Math.min(PAGINATOR_LOOKAROUND, pagesCount);
        int rightLimit = Math.max(0, pagesCount - PAGINATOR_LOOKAROUND);

        SortedSet<Integer> pageNumbers = new TreeSet<>();
        for (int i = 0; i < leftLimit; i++) {
            pageNumbers.add(i);
        }

        for (int i = pageNumber - PAGINATOR_LOOKAROUND + 1; i < pageNumber + PAGINATOR_LOOKAROUND && i > 0 && i < pagesCount; i++) {
            pageNumbers.add(i);
        }

        for (int i = rightLimit; i < pagesCount; i++) {
            pageNumbers.add(i);
        }

        return pageNumbers;
    }

    private static class PageInfo {
        private int pagesCount;
        private int pageNumber;

        public PageInfo(Page page) {
            pagesCount = (int) Math.ceil(1.0 * page.getTotalElements() / page.getSize());
            pageNumber = page.getNumber();
        }

        public int getPagesCount() {
            return pagesCount;
        }

        public int getPageNumber() {
            return pageNumber;
        }
    }

}
