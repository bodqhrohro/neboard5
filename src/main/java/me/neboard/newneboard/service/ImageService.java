package me.neboard.newneboard.service;

import me.neboard.newneboard.abstracts.Dimensions;
import me.neboard.newneboard.abstracts.FileMetadata;
import me.neboard.newneboard.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Service
public class ImageService {
    private static final String PREFIX_THUMB = "thumb_";
    private static final String DIR_STATIC = "static";

    @Value("${image.thumb.width}")
    private int thumbWidth;

    @Value("${image.thumb.height}")
    private int thumbHeight;

    @Autowired
    private FileService fileService;

    @Autowired
    private UtilService utilService;

    public boolean canCreateThumb(String mimetype) {
        return utilService.isImage(mimetype);
    }

    public FileMetadata createThumb(String filename) throws BusinessException {
        Map<String, Object> options = new HashMap<>();
        options.put("source", filename);
        options.put("type", "thumb");
        options.put("thumbPrefix", PREFIX_THUMB);
        options.put("thumbWidth", thumbWidth);
        options.put("thumbHeight", thumbHeight);
        return fileService.create(options);
    }

    public String getThumbName(String originalImageName) {
        return PREFIX_THUMB + originalImageName;
    }

    @Cacheable(key = "#filename", value = "imageSizeClasspath")
    public Dimensions getStaticImageSizeFromClasspath(String filename) throws BusinessException {
        BufferedImage bimg;
        try (InputStream is = new ClassPathResource(new File(DIR_STATIC, filename).getPath()).getInputStream()) {
            bimg = ImageIO.read(is);
        } catch (IOException e) {
            throw new BusinessException(e);
        }
        return getDimensions(bimg);
    }

    private Dimensions getDimensions(BufferedImage bimg) {
        int width = bimg.getWidth();
        int height = bimg.getHeight();

        return new Dimensions(width, height);
    }

}
