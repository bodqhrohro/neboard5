package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.service.FormAttachmentService;
import me.neboard.newneboard.creation.attachment.FormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.repository.AttachmentRepository;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.CacheService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.ViewService;
import me.neboard.newneboard.viewer.AttachmentViewer;
import me.neboard.newneboard.web.ExternalImageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service("attachmentService")
@Transactional
public class AttachmentServiceImpl implements AttachmentService {
    @Autowired
    private AttachmentRepository attachmentRepository;

    @Autowired
    private FileService fileService;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private ViewService viewService;

    @Autowired
    private FormAttachmentService formAttachmentService;

    @Override
    @CacheEvict(value = "missingFiles", allEntries = true)
    public List<Attachment> createFromFormData(List<FormAttachment> formAttachments) throws BusinessException {
        List<Attachment> attachments = new ArrayList<>();

        for (FormAttachment formAttachment : formAttachments) {
            Attachment attachment = formAttachmentService.getCreator(formAttachment).getOrCreate(formAttachment);
            if (attachment != null) {
                attachments.add(attachment);
            }
        }

        return attachments;
    }

    @Override
    public String getView(Attachment attachment) throws BusinessException {
        AttachmentViewer viewer = viewService.getViewer(attachment);

        String cacheKey = cacheService.getKey(viewer.getClass().getName(), attachment.getId());
        String result = cacheService.get(CACHE_VIEW_TEMPLATE, cacheKey,
                () -> viewer.getView(attachment), String.class);

        return viewer.replaceData(result, attachment);
    }

    @Override
    @Cacheable(value = CACHE_ATTACH_COUNT_BY_THREAD, key = "#thread.getId()")
    public long getCountByThread(PostThread thread) {
        return attachmentRepository.countByPostAttachmentsPostThread(thread);
    }

    @Override
    @Cacheable(value = "missingFiles")
    public Set<String> getMissingFiles() {
        return attachmentRepository.findByUrlNull().stream()
                .map(Attachment::getFilename)
                .filter(fileService::fileExists)
                .collect(Collectors.toSet());
    }

    @Override
    public List<String> getFileUrlList() {
        return attachmentRepository.findByUrlNull().stream()
                .map(this::getUrl)
                .collect(Collectors.toList());
    }

    @Override
    public long getCount() {
        return attachmentRepository.count();
    }

    @Override
    public Attachment findByFilename(String filename) {
        return attachmentRepository.findByFilename(filename);
    }

    @Override
    public Attachment save(Attachment attachment) {
        return attachmentRepository.save(attachment);
    }

    @Override
    public String getUrl(Attachment attachment) {
        return attachment.isFileAttachment() ?
                ExternalImageController.URL_FILES + attachment.getFilename() :
                attachment.getUrl();
    }

//    /**
//     * Remove attachments that are no longer referenced by any post
//     */
//    @Scheduled(cron = "0 0 0 * * ?")
//    public void removeUnused() throws BusinessException {
//        /*
//         * TODO Clean up files without attachments
//         * This can be split into 2 operations:
//         * 1. Remove unused attachments but don't touch the files
//         * 2. Go through files and remove the ones not linked to
//         * any attachment
//         */
//        LOGGER.info("Removing unused attachments");
//        List<Attachment> attachments = attachmentRepository.findWithoutPosts();
//        for (Attachment attachment : attachments) {
//            if (!attachment.isUrlAttachment()) {
//                fileService.delete(attachment.getFilename());
//            }
//
//            attachmentRepository.delete(attachment);
//            LOGGER.info(String.format("Removed attachment %s", attachment));
//        }
//    }

    @Override
    public Attachment findByUrl(String url) {
        return attachmentRepository.findByUrl(url);
    }

    @Override
    public List<Attachment> findByThread(PostThread thread) {
        return attachmentRepository.findDistinctByPostAttachmentsPostThread(thread);
    }
}
