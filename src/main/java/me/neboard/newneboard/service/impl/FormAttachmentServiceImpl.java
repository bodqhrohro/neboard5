package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.creation.AttachmentCreator;
import me.neboard.newneboard.creation.attachment.*;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.service.FormAttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

@Service
public class FormAttachmentServiceImpl implements FormAttachmentService {
    @Autowired
    private List<AttachmentCreator> creators;

    @Override
    public <T extends FormAttachment> AttachmentCreator<T> getCreator(T source) {
        return creators.stream().filter(creator -> creator.handles(source))
                .findFirst().orElseThrow(UnsupportedOperationException::new);
    }

    @Override
    public FormAttachment getAttachment(Object source) {
        FormAttachment attachment;
        if (source instanceof MultipartFile) {
            attachment = new MultipartFileFormAttachment((MultipartFile) source);
        } else if (source instanceof File) {
            attachment = new FileFormAttachment((File) source);
        } else if (source instanceof String) {
            attachment = new UrlFormAttachment((String) source);
        } else if (source instanceof Attachment) {
            attachment = new AttachmentFormAttachment((Attachment) source);
        } else {
            throw new UnsupportedOperationException();
        }
        return attachment;
    }
}
