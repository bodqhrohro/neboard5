package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.abstracts.FileMetadata;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.CacheService;
import me.neboard.newneboard.service.ExternalService;
import me.neboard.newneboard.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Service("fileService")
public class FileServiceImpl implements FileService {

    private static final String CACHE_FILE_METADATA = "fileMetadata";

    @Autowired
    private ExternalService externalService;

    @Autowired
    private CacheService cacheService;

    @Override
    public boolean fileExists(String filename) {
        try {
            externalService.request(
                    externalService.getFileServiceUrl(filename + "/"),
                    HttpMethod.HEAD, null, null, Void.class);
            return true;
        } catch (BusinessException e) {
            return false;
        }
    }

    @Override
    public <T> T getMetadata(String filename, Class<T> clazz) throws BusinessException {
        String key = cacheService.getKey(filename, clazz.getName());
        return cacheService.get(CACHE_FILE_METADATA, key, () ->
                externalService.request(externalService.getFileServiceUrl(filename + "/metadata/"),
                        HttpMethod.GET, null, null, clazz), clazz);
    }

    @Override
    public long getFileSize(String filename) throws BusinessException {
        return getMetadata(filename, FileMetadata.class).getSize();
    }

    @Override
    public String getMimetype(String filename) throws BusinessException {
        return getMetadata(filename, FileMetadata.class).getMimetype();
    }

    @Override
    public void delete(String filename) throws BusinessException {
        externalService.request(
                externalService.getFileServiceUrl(filename + "/"),
                HttpMethod.DELETE, null, null, Void.class);
    }

    @Override
    public FileMetadata create(InputStream is, String originalFilename) throws BusinessException {
        Map<String, Object> data = new HashMap<>();

        ContentDisposition contentDisposition = ContentDisposition
                .builder("form-data")
                .name("file")
                .filename(originalFilename)
                .build();
        MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());

        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            is.transferTo(os);

            HttpEntity<byte[]> fileEntity = new HttpEntity<>(os.toByteArray(), fileMap);
            data.put("file", fileEntity);
        } catch (IOException e) {
            throw new BusinessException(e);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        return externalService.request(
                externalService.getFileServiceUrl(),
                HttpMethod.POST, data, headers, FileMetadata.class);
    }

    @Override
    public FileMetadata create(Map<String, Object> options) throws BusinessException {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        String response = externalService.rawRequest(
                externalService.getFileServiceUrl(),
                HttpMethod.POST, externalService.toJson(options), headers);
        return externalService.fromJson(response, FileMetadata.class);
    }

}
