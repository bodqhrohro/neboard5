package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.parser.ParserResult;
import me.neboard.newneboard.parser.TextParser;
import me.neboard.newneboard.service.ParserService;
import me.neboard.newneboard.service.SettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("parserService")
public class ParserServiceImpl implements ParserService {
    @Autowired
    private TextParser parser;

    @Autowired
    private SettingsService settingsService;

    @Override
    public String preparse(String rawText) throws BusinessException {
        String method = settingsService.getParseMethod();
        return parser.preparse(method, rawText);
    }

    @Override
    public ParserResult parse(String rawText) throws BusinessException {
        String method = settingsService.getParseMethod();
        return parser.parse(method, rawText);
    }

    @Override
    public String getPanelText() throws BusinessException {
        String method = settingsService.getParseMethod();
        return parser.getPanelText(method);
    }

    @Override
    public List<String> getMethods() throws BusinessException {
        return parser.getMethods();
    }

    @Override
    public int getLineBreaks() {
        // TODO Get from parse method definition
        String method = settingsService.getParseMethod();
        return "markdown".equals(method) ? 2 : 1;
    }

}
