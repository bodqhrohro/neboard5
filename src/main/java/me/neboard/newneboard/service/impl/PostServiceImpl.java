package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.domain.*;
import me.neboard.newneboard.domain.manytomany.PostAttachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.PermissionException;
import me.neboard.newneboard.form.CreatableForm;
import me.neboard.newneboard.parser.ParserResult;
import me.neboard.newneboard.repository.PostRepository;
import me.neboard.newneboard.service.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.ObjectDeletedException;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service("postService")
@Transactional
public class PostServiceImpl implements PostService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PostService.class);

	private static final String LOG_THREAD = "Created thread %s with title %s and %d attachments";
	private static final String LOG_POST = "Created post %s in thread %s with %d attachments";

	private static final String HTML_LINK = "<a href='%s'>&gt;&gt;%d</a>";

	private static final String URL_THREAD = "/thread/";

	private static final String CACHE_POST_COUNT_BY_THREAD = "postCountByThread";
	private static final String CACHE_POST_REFMAP = "postRefmap";
	private static final String CACHE_POST_HTML_LINK = "postHtmlLink";
	private static final String CACHE_POST_URL = "postUrl";

	@Value("${thread.post.limit}")
	private int postLimit;

	@Value("${short.title.length}")
	private int shortTitleLength;

	@Autowired
	private ThreadService threadService;

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private TagService tagService;

	@Autowired
	private AttachmentService attachmentService;

	@Autowired
	private BanService banService;

	@Autowired
	private ParserService parserService;

	@Autowired
	private UtilService utilService;

	@Autowired
	private CacheService cacheService;

	@Autowired
	private TripcodeService tripcodeService;

	@Override
	public Post create(PostCreateDetails details) throws BusinessException {
		Ban ban = banService.findByAddress(details.getPosterAddress());
		if (ban != null) {
			throw new PermissionException("Banned for reason: " + ban.getReason());
		}

		boolean opening = details.isOpening();
		PostThread thread;
		if (opening) {
			Set<Tag> tags = tagService.getTagsFromNames(details.getTagNames());
			thread = new PostThread(tags, postLimit);
			thread = threadService.save(thread);
		} else {
			thread = details.getThread();
			if (!thread.canPost()) {
				throw new BusinessException("Cannot post into archived thread");
			}
		}

		String preparsedRawText = parserService.preparse(details.getText());
		ParserResult parserResult = parserService.parse(preparsedRawText);
		List<Attachment> attachments = details.getAttachments();
		Post post = new Post(details.getTitle(), preparsedRawText, parserResult.getText(),
				new Date(), thread, opening, getPostAttachments(attachments),
				details.getPosterAddress(), details.getTripcode());

		post = save(post);
		connectReplies(post, parserResult.getReflinks());

		threadService.bump(thread, post.getPubTime());

		if (opening) {
			LOGGER.info(String.format(LOG_THREAD, post.toString(),
					getShortTitle(post), attachments.size()));
		} else {
			LOGGER.info(String.format(LOG_POST, post.toString(),
					post.getThread().getOpeningPost().toString(),
					attachments.size()));
		}

		return post;
	}

	private List<PostAttachment> getPostAttachments(List<Attachment> attachments) {
		List<PostAttachment> postAttachments = new ArrayList<>();
		int order = 0;

		// FIXME Currently only one post-attachment mapping can be stored for
		// a post and attachment
		Set<Long> processed = new HashSet<>();
		for (Attachment attachement : attachments) {
			if (!processed.contains(attachement.getId())) {
				postAttachments.add(new PostAttachment(attachement, order));
				processed.add(attachement.getId());
				order++;
			}
		}

		return postAttachments;
	}

	@Override
	public Post findById(long id) {
		return postRepository.findById(id).orElse(null);
	}

	@Override
	public List<Post> findByPosterAddress(String address) {
		return postRepository.findByPosterAddress(address);
	}

	@Override
	public double getPostSpeed() {
		LocalDate dateBefore = LocalDate.now().minusWeeks(1);

		return postRepository.countByPubTimeGreaterThan(java.sql.Date.valueOf(dateBefore)) / 7d;
	}

	@Override
	@Cacheable(value = CACHE_POST_COUNT_BY_THREAD, key = "#thread.getId()")
	public int getCountByThread(PostThread thread) {
		return postRepository.countByThread(thread);
	}

	@Override
	public <T> List<T> findByThreadAndIdIn(PostThread thread, List<Long> ids, Class<T> type) {
		return postRepository.findByThreadAndIdIn(thread, ids, type);
	}

	@Override
	public <T> List<T> findByThread(PostThread thread, Class<T> type) {
		return postRepository.findByThread(thread, type);
	}

	@Override
	public Page<Post> findByExample(Pageable pageable, Example<Post> example) {
		return postRepository.findAll(example, pageable);
	}

	@Override
	public Page<Post> findByAttachmentFilename(Pageable pageable, String filename) {
		return postRepository.findByPostAttachmentsAttachmentFilename(pageable, filename);
	}

	@Override
	public Page<Post> search(Pageable pageable, String query) {
		Page<Post> page;
		if (StringUtils.isBlank(query)) {
			page = new PageImpl<>(Collections.emptyList());
		} else {
			page = postRepository.findByTitleContainingIgnoreCaseOrTextContainingIgnoreCase(pageable, query, query);
		}
		return page;
	}

	@Override
	public void delete(Post post) {
		PostThread thread = post.getThread();

		evictPostCaches(post, thread);
		cacheService.evict(CACHE_POST_URL, post.getId());
		cacheService.evict(CACHE_POST_HTML_LINK, post.getId());

		Collection<Post> referencedPosts = post.getReferencedPosts();

		postRepository.delete(post);

		for (Post refPost : referencedPosts) {
			cacheService.evict(CACHE_POST_REFMAP, refPost.getId());
		}

		// Return the thread from bumplimit if it was not reached before deletion
		if (thread.getStatus() == PostThread.Status.BUMPLIMIT && getCountByThread(thread) < thread.getPostLimit()) {
			thread.setStatus(PostThread.Status.ACTIVE);
			threadService.save(thread);
		}
	}

	private void evictPostCaches(Post post, PostThread thread) {
		cacheService.evict(CACHE_POST_COUNT_BY_THREAD, thread.getId());
		cacheService.evict(AttachmentService.CACHE_ATTACH_COUNT_BY_THREAD, thread.getId());
		cacheService.evict(CACHE_POST_REFMAP, post.getId());
	}

	@Override
	@Cacheable(value = CACHE_POST_URL, key = "#post.getId()")
	public String getUrl(Post post) {
		return post.isOpening() ? URL_THREAD + post.getId() : URL_THREAD + post.getThread().getOpeningPost().getId() + "#" + post.getId();
	}

	@Override
	@Cacheable(value = CACHE_POST_REFMAP, key = "#post.getId()")
	public String getReplyLinks(Post post) {
		return post.getReplies().stream().map(this::getHtmlLink).collect(Collectors.joining(IEntity.DELIMITER));
	}

	@Override
	@Cacheable(value = CACHE_POST_HTML_LINK, key = "#post.getId()")
	public String getHtmlLink(Post post) {
		String link = String.format(HTML_LINK, getUrl(post), post.getId());
		if (post.isOpening()) {
			link = "<b>" + link + "</b>";
		}
		return link;
	}

	@Override
	public List<Post> findTodayActiveThreads(Tag tag) {
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime nowMinusDay = now.minusDays(1);
		Date dayDate = Date.from(nowMinusDay.atZone(ZoneId.systemDefault()).toInstant());

		List<Post> todayOpenings;

		if (tag == null) {
			todayOpenings = postRepository.findByOpeningAndThreadBumpTimeGreaterThanOrderByIdDesc(true, dayDate);
		} else {
			todayOpenings = postRepository.findByOpeningAndThreadTagsInAndThreadBumpTimeGreaterThanOrderByIdDesc(true, Collections.singletonList(tag), dayDate);
		}

		return todayOpenings;
	}

	@Override
	public Post updatePost(long postId, CreatableForm form) throws BusinessException {
		Post post = findById(postId);

		String text = form.getText();
		String preparsedRawText = parserService.preparse(text);

		post.setRawText(preparsedRawText);
		ParserResult parserResult = parserService.parse(preparsedRawText);
		post.setText(parserResult.getText());

		post.setTitle(form.getTitle());

		save(post);
		connectReplies(post, parserResult.getReflinks());

		String[] tagNames = form.getTagNames();
		if (tagNames != null) {
			Set<Tag> tags = tagService.getTagsFromNames(tagNames);

			PostThread thread = post.getThread();

			thread.getTags().clear();
			thread.getTags().addAll(tags);

			threadService.save(thread);
		}

		return post;
	}

	@Override
	public void purgeFromPost(Post post) {
		List<Post> posts = findByPosterAddress(post.getPosterAddress(), post.getId());
		purgePosts(posts, false);
	}

	@Override
	public void purgePosts(List<Post> posts, boolean ban) {
		for (Post post : posts) {
			try {
				if (ban) {
					banService.banUser(post.getPosterAddress(), "Post #" + post.getId());
				}
				if (post.isOpening()) {
					threadService.delete(post.getThread());
				} else {
					delete(post);
				}
			} catch (EntityNotFoundException | ObjectDeletedException e) {
				// Don't bother, this means we removed the post's thread earlier
			}
		}
	}

	@Override
	public long getCount() {
		return postRepository.count();
	}

	@Override
	public Collection<Post> getThreadLastPosts(PostThread thread) {
		Pageable lastPosts = utilService.getPageable(Optional.of(0),
				threadService.getLastPostsCount(), Sort.by(Sort.Order.desc("id")));
		Page<Post> page = postRepository.findByThreadAndOpening(lastPosts, thread, false);

		List<Post> result = new ArrayList<>(page.getContent());
		Collections.reverse(result);

		return result;
	}

	@Override
	public List<Attachment> getAttachments(Post post) {
		return post.getPostAttachments()
				.stream()
				.map(PostAttachment::getAttachment)
				.collect(Collectors.toList());
	}

	@Override
	public PostCreateDetails.PostCreateDetailsBuilder getCreateDetailsBuilder() {
		return new PostCreateDetails.PostCreateDetailsBuilder(attachmentService,
				tripcodeService);
	}

	@Override
	public String getShortTitle(Post post) {
		String title = post.getTitle();
		if (org.thymeleaf.util.StringUtils.isEmpty(title)) {
			title = Jsoup.parse(post.getText()).text();
			title = org.thymeleaf.util.StringUtils.abbreviate(title, shortTitleLength);
		}
		return title;
	}

	private void connectReplies(Post post, List<Long> reflinks) {
		for (Long postId : reflinks) {
			if (!postId.equals(post.getId())) {
				Post referencedPost = findById(postId);
				if (referencedPost != null) {
					referencedPost.getReplies().add(post);
					save(referencedPost);
				}
			}
		}
	}

	private Post save(Post post) {
		evictPostCaches(post, post.getThread());

		post.setUuid(UUID.randomUUID().toString());
		return postRepository.save(post);
	}

	private List<Post> findByPosterAddress(String address, long firstPostId) {
		return postRepository.findByPosterAddressAndIdGreaterThanEqual(address, firstPostId);
	}

}
