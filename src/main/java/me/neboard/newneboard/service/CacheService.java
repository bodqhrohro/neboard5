package me.neboard.newneboard.service;

import me.neboard.newneboard.abstracts.Action;
import me.neboard.newneboard.exception.BusinessException;

public interface CacheService {
    void evict(String cacheName, Object key);

    <T> T get(String cacheName, Object key, Action<T> action, Class<T> clazz) throws BusinessException;
    <T> T get(String cacheName, Object key, Class<T> clazz);

    void put(String cacheName, Object key, Object value);

    void clear(String cacheName);

    String getKey(Object... keys);
}
