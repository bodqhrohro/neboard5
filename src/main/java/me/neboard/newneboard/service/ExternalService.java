package me.neboard.newneboard.service;

import com.google.gson.Gson;
import me.neboard.newneboard.abstracts.ExternalCacheMeta;
import me.neboard.newneboard.abstracts.ExternalImageData;
import me.neboard.newneboard.abstracts.TimeResult;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.ExternalRequestException;
import me.neboard.newneboard.exception.NotFoundException;
import me.neboard.newneboard.parser.ExternalParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ExternalService {
    private static final String EXT_URL_DOMAIN_IMAGE = "%s/domains/image/%s";
    private static final String EXT_URL_FILE_IMAGE = "%s/files/image/%s";

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalService.class);

    private static final String CACHE_DOMAIN_IMAGE = "domainImage";
    private static final String CACHE_FILE_IMAGE = "fileImage";

    @Value("${external.image.host}")
    private String imageServiceHost;

    @Value("${external.parser.host}")
    private String parserServiceHost;

    @Value("${external.file.host}")
    private String fileServiceHost;

    @Autowired
    private UtilService utilService;

    @Autowired
    private CacheService cacheService;

    private final List<ExternalCacheMeta> caches = new ArrayList<>();

    private final RestTemplate restTemplate = new RestTemplate();

    private final Gson gson = new Gson();

    @PostConstruct
    private void init() {
        caches.add(new ExternalCacheMeta(CACHE_DOMAIN_IMAGE, getImageServiceUrl() + "/last-update"));
        caches.add(new ExternalCacheMeta(CACHE_FILE_IMAGE, getImageServiceUrl() + "/last-update"));
        caches.add(new ExternalCacheMeta(ExternalParser.CACHE_PARSER_METHODS, getParserServiceUrl() + "/last-update"));
    }

    public String getImageServiceUrl() {
        return imageServiceHost;
    }

    public String getParserServiceUrl() {
        return parserServiceHost;
    }

    public String getFileServiceUrl() {
        return fileServiceHost;
    }

    public String getFileServiceUrl(String relativeUrl) {
        return getFileServiceUrl() + "/" + relativeUrl;
    }

    @Cacheable(value = CACHE_DOMAIN_IMAGE, key = "#domain")
    public ExternalImageData getDomainImage(String domain) throws BusinessException {
        try {
            return requestGet(
                    String.format(EXT_URL_DOMAIN_IMAGE, getImageServiceUrl(), domain),
                    null,
                    ExternalImageData.class);
        } catch (NotFoundException e) {
            return null;
        }
    }

    @Cacheable(value = CACHE_FILE_IMAGE, key = "#mimetype")
    public ExternalImageData getFileImage(String mimetype) throws BusinessException {
        try {
            return requestGet(
                    String.format(EXT_URL_FILE_IMAGE, getImageServiceUrl(), mimetype),
                    null,
                    ExternalImageData.class);
        } catch (NotFoundException e) {
            return null;
        }
    }

    public <T> T requestPost(String url, Map<String, Object> data, Class<T> resultClass) throws BusinessException {
        return request(url, HttpMethod.POST, data, null, resultClass);
    }

    public <T> T requestGet(String url, Map<String, Object> data, Class<T> resultClass) throws BusinessException {
        return request(url, HttpMethod.GET, data, null, resultClass);
    }

    public <T> T request(String url, HttpMethod method, Map<String, Object> data, HttpHeaders headers, Class<T> resultClass) throws BusinessException {
        MultiValueMap<String, Object> formData = null;
        if (data != null && !data.isEmpty()) {
            formData = new LinkedMultiValueMap<>();
            data.forEach(formData::add);
        }

        HttpEntity<Object> requestEntity = null;
        if (formData != null) {
            requestEntity = new HttpEntity<>(formData, headers);
        }
        return getObjectFromRequest(url, method, requestEntity, resultClass);
    }

    public String rawRequest(String url, HttpMethod method, Object body, HttpHeaders headers) throws BusinessException {
        HttpEntity<Object> requestEntity = null;
        if (body != null) {
            requestEntity = new HttpEntity<>(body, headers);
        }
        return getStringFromRequest(url, method, requestEntity);
    }

    public <T> ResponseEntity<T> gatewayGet(URI thirdPartyApi, Class<T> resultClass) {
        return restTemplate.exchange(thirdPartyApi,
                HttpMethod.GET, null,
                resultClass);
    }

    public <T> ResponseEntity<T> gatewayGet(URI thirdPartyApi, HttpHeaders headers, Class<T> resultClass) {
        return restTemplate.exchange(thirdPartyApi,
                HttpMethod.GET, new HttpEntity<>(headers),
                resultClass);
    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void clearExternalCaches() throws BusinessException {
        for (ExternalCacheMeta cacheMeta : caches) {
            TimeResult response = requestGet(cacheMeta.getCacheUpdateUrl(), null, TimeResult.class);
            if (response != null) {
                String time = response.getTime();
                if (time != null && !time.equals(cacheMeta.getCacheUpdateTime())) {
                    LOGGER.info(String.format("Clearing external cache %s as the update time has changed", cacheMeta.getCacheName()));
                    cacheService.clear(cacheMeta.getCacheName());
                    cacheMeta.setCacheUpdateTime(time);
                }
            }
        }
    }

    public <T> T fromJson(String json, Class<T> type) {
        T result = null;
        if (json != null) {
            result = gson.fromJson(json, type);
        }
        return result;
    }

    public String toJson(Object obj) {
        return gson.toJson(obj);
    }

    private String getStringFromRequest(String url, HttpMethod method, HttpEntity requestEntity) throws BusinessException {
        return getObjectFromRequest(url, method, requestEntity, String.class);
    }

    private <T> T getObjectFromRequest(String url, HttpMethod method, HttpEntity requestEntity,
                                       Class<T> responseType) throws NotFoundException, ExternalRequestException {
        ResponseEntity<T> response;
        try {
            response = restTemplate.exchange(url, method, requestEntity,
                    responseType);
        } catch (HttpClientErrorException.NotFound e) {
            throw new NotFoundException("Nothing found from request " + url);
        } catch (HttpClientErrorException | ResourceAccessException e) {
            throw new ExternalRequestException(e);
        }

        return response.getBody();
    }

}
