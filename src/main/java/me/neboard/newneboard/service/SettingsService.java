package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.domain.User;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.PermissionException;
import me.neboard.newneboard.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@EnableScheduling
public class SettingsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SettingsService.class);

    private static final String DEFAULT_THEME = "md";

    private static final String ATTR_USER_ID = "userUuid";
    private static final String DELIMITER_THEME = ":";

    @Value("#{'${themes}'.split(',')}")
    private List<String> themes;

    @Value("${users.timeout}")
    private Duration timeout;

    @Value("#{'${admin.uuid}'.split(',')}")
    private List<String> adminUuids;

    @Value("${text.parser.method}")
    private String defaultParseMethod;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    @Autowired
    private UserRepository userRepository;

    public Map<String, String> getThemes() {
        return themes.stream().map(theme -> theme.split(DELIMITER_THEME)).collect(Collectors.toMap(theme -> theme[0], theme -> theme[1]));
    }

    public String getTheme() {
        return getTheme(getOrCreateUser());
    }

    public String getTheme(User user) {
        return Objects.requireNonNullElse(user.getTheme(), DEFAULT_THEME);
    }

    public void saveTheme(String theme) {
        User user = getOrCreateUser();
        user.setTheme(theme);
        save(user);
    }

    public Set<Tag> getFavoriteTags() {
        return getOrCreateUser().getFavoriteTags();
    }

    public Set<Tag> getHiddenTags() {
        return getOrCreateUser().getHiddenTags();
    }

    public void addFavoriteTag(Tag tag) {
        User user = getOrCreateUser();

        user.getFavoriteTags().add(tag);
        save(user);
    }

    public void removeFavoriteTag(Tag tag) {
        User user = getOrCreateUser();

        user.getFavoriteTags().remove(tag);
        save(user);
    }

    public void addHiddenTag(Tag tag) {
        User user = getOrCreateUser();

        user.getHiddenTags().add(tag);
        save(user);
    }

    public void removeHiddenTag(Tag tag) {
        User user = getOrCreateUser();

        user.getHiddenTags().remove(tag);
        save(user);
    }

    public User getOrCreateUser() {
        String uuid = getCookie(ATTR_USER_ID);

        User user = null;
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
        } else {
            user = findByUuid(uuid);
        }

        if (user == null) {
            user = save(new User(uuid));
            LOGGER.debug(String.format("Created user %s", user));
            setCookie(ATTR_USER_ID, uuid);
        }

        return user;
    }

    public User findByUuid(String uuid) {
        return userRepository.findByUuid(uuid);
    }

    public void login(String uuid) throws BusinessException {
        // Login only when not the same user we are using now
        if (!uuid.equals(getCookie(ATTR_USER_ID))) {
            User user = findByUuid(uuid);
            if (user != null) {
                setCookie(ATTR_USER_ID, uuid);
                user = save(user);
                LOGGER.info("Login performed for user " + user);
            } else {
                throw new BusinessException("No such user");
            }
        }
    }

    public User save(User user) {
        user.setLastOperatedTime(new Date());
        return userRepository.save(user);
    }

    public boolean isTagFavorite(Tag tag) {
        Set<Tag> favoriteTags = getOrCreateUser().getFavoriteTags();
        if (favoriteTags == null) {
            return false;
        } else {
            return favoriteTags.contains(tag);
        }
    }

    public boolean isTagHidden(Tag tag) {
        Set<Tag> tags = getOrCreateUser().getHiddenTags();
        if (tags == null) {
            return false;
        } else {
            return tags.contains(tag);
        }
    }

    public boolean isAdmin(User user) {
        return adminUuids.contains(user.getUuid());
    }

    public boolean isAdmin() {
        return isAdmin(getOrCreateUser());
    }

    public void validatePermission() throws PermissionException {
        if (!isAdmin()) {
            throw new PermissionException("Only admin can do that");
        }
    }

    public TimeZone getTimezone() {
        return getTimezone(getOrCreateUser());
    }

    public TimeZone getTimezone(User user) {
        String tzId = user.getTimezone();
        return tzId == null ? TimeZone.getDefault() : TimeZone.getTimeZone(tzId);
    }

    public void saveTimezone(TimeZone timezone) {
        User user = getOrCreateUser();
        user.setTimezone(timezone.getID());
        save(user);
    }

    public void saveParseMethod(String parseMethod) {
        User user = getOrCreateUser();
        user.setParseMethod(parseMethod);
        save(user);
    }

    @Scheduled(cron = "0 0 * * * ?")
    public void removeInactiveUsers() {
        LocalDate oldestUserDate = LocalDate.now().minusDays(timeout.toDays());

        LOGGER.debug("Started user cleanup");
        long deletedCount = userRepository.deleteByLastOperatedTimeLessThan(java.sql.Date.valueOf(oldestUserDate));

        if (deletedCount > 0) {
            LOGGER.info(String.format("Deleted %s users", deletedCount));
        }
    }

    public String getCookie(String name) {
        Cookie[] cookies = request.getCookies();
        String value = null;

        if (cookies != null) {
            value = Arrays.stream(cookies)
                    .filter(cookie -> name.equals(cookie.getName()))
                    .findFirst()
                    .map(Cookie::getValue)
                    .orElse(null);
        }

        return value;
    }

    public void setCookie(String name, String value) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(Integer.MAX_VALUE);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    public String getParseMethod() {
        return Objects.requireNonNullElse(getOrCreateUser().getParseMethod(),
                defaultParseMethod);
    }
}
