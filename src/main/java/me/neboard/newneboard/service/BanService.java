package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.Ban;
import me.neboard.newneboard.repository.BanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BanService {
    @Autowired
    private BanRepository banRepository;

    public Ban findByAddress(String address) {
        return banRepository.findByAddress(address);
    }

    public Ban banUser(String address, String reason) {
        Ban ban = findByAddress(address);
        if (ban == null) {
            ban = new Ban(address, reason);
            ban = banRepository.save(ban);
        }
        return ban;
    }
}
