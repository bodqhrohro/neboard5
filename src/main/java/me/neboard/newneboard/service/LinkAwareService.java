package me.neboard.newneboard.service;

public interface LinkAwareService<T> {
    String getUrl(T entity);
    String getHtmlLink(T entity);
}
