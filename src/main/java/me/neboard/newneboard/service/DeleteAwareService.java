package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.IEntity;

public interface DeleteAwareService<T extends IEntity> {
    void delete(T entity);
}
