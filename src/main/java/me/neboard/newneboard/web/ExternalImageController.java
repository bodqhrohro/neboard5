package me.neboard.newneboard.web;

import me.neboard.newneboard.service.ExternalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

@Controller
public class ExternalImageController {
    private static final String URL_STICKERS_SEARCH = "/api/stickers/{query}";

    public static final String URL_FILES = "/files/";

    private static final String URL_FILE = URL_FILES + "{name}";

    private static final String EXT_URL_STICKERS = "%s/stickers/%s";

    @Autowired
    private ExternalService externalService;

    @GetMapping(URL_STICKERS_SEARCH)
    @ResponseBody
    public ResponseEntity<String> searchStickers(@PathVariable String query) throws URISyntaxException {
        URI thirdPartyApi = new URI(String.format(EXT_URL_STICKERS, externalService.getImageServiceUrl(), query));

        return externalService.gatewayGet(thirdPartyApi, String.class);
    }

    @GetMapping(URL_FILE)
    @ResponseBody
    public ResponseEntity<Resource> getFile(@RequestHeader HttpHeaders headers,
            @PathVariable String name) throws URISyntaxException {
        URI thirdPartyApi = new URI(externalService.getFileServiceUrl(name));

        return externalService.gatewayGet(thirdPartyApi, headers, Resource.class);
    }

    private String extractRecursivePathVariable(HttpServletRequest request) {
        final String path =
                request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).toString();
        final String bestMatchingPattern =
                request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE).toString();

        return new AntPathMatcher().extractPathWithinPattern(bestMatchingPattern, path);
    }

}
