package me.neboard.newneboard.web;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.PermissionException;
import me.neboard.newneboard.form.PostForm;
import me.neboard.newneboard.form.ThreadForm;
import me.neboard.newneboard.service.BanService;
import me.neboard.newneboard.service.PostService;
import me.neboard.newneboard.service.SettingsService;
import me.neboard.newneboard.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(AdminController.URL)
public class AdminController {
    public static final String URL = "/admin";

    private static final String URL_PURGE = "/purge/{posterAddress}";
    private static final String URL_CLEANUP = "/cleanup/{text}";
    private static final String URL_EDIT_POST = "/edit/post/{postId}";
    private static final String URL_EDIT_THREAD = "/edit/thread/{postId}";

    @Autowired
    private PostService postService;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private BanService banService;

    private void deletePostOrThread(Post post) {
        if (post.isOpening()) {
            threadService.delete(post.getThread());
        } else {
            postService.delete(post);
        }
    }

   @PostMapping(URL_PURGE)
    public String purge(@PathVariable String posterAddress, @RequestParam long postId,
                        HttpServletRequest request) throws PermissionException {
        settingsService.validatePermission();

        Post post = postService.findById(postId);
        banService.banUser(posterAddress, "Post #" + post.getId());

        String returnUrl = getBackRedirect(post);
        postService.purgeFromPost(post);

        return returnUrl;
    }

    @PostMapping(URL_CLEANUP)
    public String cleanup(@PathVariable String text, HttpServletRequest request)
        throws PermissionException {
        settingsService.validatePermission();

        List<Post> posts = postService.search(text);
        postService.purgePosts(posts, true);

        return getBackRedirect(request);
    }

    private String getBackRedirect(Post post) {
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX +
                (post.isOpening() ? "/" : postService.getUrl(post.getThread().getOpeningPost()));
    }

    private String getBackRedirect(HttpServletRequest request) {
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX + request.getHeader("referer");
    }

    @GetMapping(URL_EDIT_POST)
    public String editPost(@PathVariable long postId,
                           @ModelAttribute("form") PostForm form,
                           Model model) throws PermissionException {
        settingsService.validatePermission();

        Post post = postService.findById(postId);
        model.addAttribute("post", post);
        model.addAttribute("thread", post.getThread());

        form.setText(post.getRawText());
        form.setTitle(post.getTitle());

        return "edit";
    }

    @GetMapping(URL_EDIT_THREAD)
    public String editThread(@PathVariable long postId,
                           @ModelAttribute("form") ThreadForm form,
                           Model model) throws PermissionException {
        settingsService.validatePermission();

        Post post = postService.findById(postId);
        model.addAttribute("post", post);

        form.setText(post.getRawText());
        form.setTitle(post.getTitle());
        form.setTags(post.getThread().getTags().stream()
                .map(Tag::getName)
                .collect(Collectors.joining(ThreadForm.DELIMITER_FORM)));

        return "edit";
    }

    @PostMapping(URL_EDIT_POST)
    public String submitPost(@PathVariable long postId,
                             @Valid @ModelAttribute("form") PostForm form,
                             BindingResult bindingResult,
                             Model model) throws BusinessException {
        settingsService.validatePermission();

        if (bindingResult.hasErrors()) {
            return editPost(postId, form, model);
        } else {
            Post post = postService.updatePost(postId, form);

            return UrlBasedViewResolver.REDIRECT_URL_PREFIX + postService.getUrl(post);
        }
    }

    @PostMapping(URL_EDIT_THREAD)
    public String submitThread(@PathVariable long postId,
                             @Valid @ModelAttribute("form") ThreadForm form,
                             BindingResult bindingResult,
                             Model model) throws BusinessException {
        settingsService.validatePermission();

        if (bindingResult.hasErrors()) {
            return editThread(postId, form, model);
        } else {
            Post post = postService.updatePost(postId, form);

            return UrlBasedViewResolver.REDIRECT_URL_PREFIX + postService.getUrl(post);
        }
    }

}
