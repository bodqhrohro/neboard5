package me.neboard.newneboard.web;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.service.PostService;
import me.neboard.newneboard.service.SettingsService;
import me.neboard.newneboard.service.TagService;
import me.neboard.newneboard.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class PostController implements PostAwareController, PageAware {
    public static final String URL_SEARCH = "/search/";
    public static final String URL_FEED = "/feed/";

    private static final String URL_GET_POST = "/post/{postId}";

    private static final String ATTR_POST = "post";
    private static final String ATTR_SHOW_BUTTONS = "showButtons";
    private static final String ATTR_POSTS = "posts";
    private static final String ATTR_TAGS = "tags";
    private static final String ATTR_SHOW_OP_INFO = "showOpInfo";
    private static final String ATTR_QUERY = "query";
    private static final String ATTR_FEED_CRITERIA = "criteria";

    private static final String MAPPING_FEED = "feed";
    private static final String MAPPING_SEARCH = "search";

    private static final String CRITERIA_TRIPCODE = "criteria.tripcode";
    private static final String CRITERIA_ADDRESS = "criteria.address";
    private static final String CRITERIA_ATTACHMENT = "criteria.attachment";

    @Autowired
    private PostService postService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private TagService tagService;

    @Autowired
    private SettingsService settingsService;

    @Value("${posts.per.page}")
    private int postsPerPage;


    // TODO Use api call and render on the client side
    @GetMapping(URL_GET_POST)
    public String getPost(@PathVariable Long postId, Model model) {
        model.addAttribute(ATTR_POST, postService.findById(postId));
        model.addAttribute(ATTR_SHOW_BUTTONS, false);

        return "post";
    }

    @GetMapping(URL_FEED)
    public String feed(@RequestParam Optional<Integer> page,
            @RequestParam Optional<String> tripcode,
            @RequestParam Optional<String> address,
            @RequestParam Optional<String> filename,
            Model model) {
        Post post = new Post();

        Map<String, String> criteria = new HashMap<>();
        if (tripcode.isPresent()) {
            post.setTripcode(tripcode.get());
            criteria.put(CRITERIA_TRIPCODE, tripcode.get());
        }

        if (settingsService.isAdmin() && address.isPresent()) {
            post.setPosterAddress(address.get());
            criteria.put(CRITERIA_ADDRESS, address.get());
        }

        Example<Post> example = Example.of(post);

        Page<Post> posts;
        Pageable byPubTime = utilService.getPageable(page,
                postsPerPage, Sort.by(Sort.Order.desc(Post.PUB_TIME)));

        if (filename.isPresent()) {
            criteria.put(CRITERIA_ATTACHMENT, filename.get());
            posts = postService.findByAttachmentFilename(byPubTime, filename.get());
        } else {
            posts = postService.findByExample(byPubTime, example);
        }

        model.addAttribute(ATTR_POSTS, posts);
        model.addAttribute(ATTR_PAGE, posts);
        model.addAttribute(ATTR_SHOW_OP_INFO, true);
        model.addAttribute(ATTR_FEED_CRITERIA, criteria);

        return MAPPING_FEED;
    }

    @GetMapping(URL_SEARCH)
    public String search(@RequestParam Optional<Integer> page,
                       @RequestParam Optional<String> query,
                       Model model) {
        Pageable pageable = utilService.getPageable(page, postsPerPage,
                Sort.by(Sort.Order.desc("pubTime")));

        String queryText = query.orElse(null);

        Page<Post> posts = postService.search(pageable, queryText);

        if (!page.isPresent()) {
            List<Tag> tags = tagService.search(queryText);
            model.addAttribute(ATTR_TAGS, tags);
        }

        model.addAttribute(ATTR_POSTS, posts);
        model.addAttribute(ATTR_PAGE, posts);
        model.addAttribute(ATTR_SHOW_OP_INFO, true);
        model.addAttribute(ATTR_QUERY, queryText);

        return MAPPING_SEARCH;
    }
}
