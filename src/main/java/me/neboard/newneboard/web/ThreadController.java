package me.neboard.newneboard.web;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.NotFoundException;
import me.neboard.newneboard.form.PostForm;
import me.neboard.newneboard.form.validation.PostFormValidator;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.PostService;
import me.neboard.newneboard.service.SettingsService;
import me.neboard.newneboard.service.UtilService;
import me.neboard.newneboard.service.impl.PostCreateDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
public class ThreadController implements PostAwareController, FormAware {
    public static final String URL_PREFIX_THREAD = "/thread/";

    private static final String URL_THREAD = URL_PREFIX_THREAD + "{openingPostId}";
    private static final String URL_GALLERY = URL_PREFIX_THREAD + "{openingPostId}/gallery";

    private static final String ATTR_THREAD = "thread";
    private static final String ATTR_UPDATE_TIMEOUT = "updateTimeout";
    private static final String ATTR_FORM_TITLE = "formTitle";
    private static final String ATTR_ATTACHMENTS = "attachments";
    private static final String ATTR_TITLE = "title";

    private static final String ATTR_MODE_SELECTOR = "modeSelector";

    private static final String MAPPING_THREAD = "thread";
    private static final String MAPPING_GALLERY = "thread_gallery";

    private static final String HTML_FORM_TITLE = "%s #%d<span class='reply-to-message' style='display: none;'> %s #<span id='reply-to-message-id'></span></span>";

    private static final String TITLE_REPLY_THREAD = "form.reply.thread";
    private static final String TITLE_REPLY_MESSAGE = "form.reply.message";

    private static final String HTML_THREAD_MODE = "<a href=\"%s\">%s</a>";
    private static final String DELIMITER_MODE = " / ";

    @Autowired
    private PostService postService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    @Qualifier("postFormValidator")
    private PostFormValidator validator;

    @Value("${thread.update.timeout}")
    private long updateTimeout;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        if (binder.getTarget() instanceof PostForm) {
            binder.setValidator(validator);
        }
    }

    @GetMapping(URL_THREAD)
    public String getThread(@PathVariable Long openingPostId, @ModelAttribute(ATTR_FORM) PostForm form, Model model,
            HttpServletResponse response) throws NotFoundException {
        String mapping;
        Post threadPost = postService.findById(openingPostId);
        if (threadPost == null) {
            throw new NotFoundException("Thread not found");
        } else if (!threadPost.isOpening()) {
            mapping = UrlBasedViewResolver.REDIRECT_URL_PREFIX + postService.getUrl(threadPost);
        } else {
            PostThread thread = threadPost.getThread();
            model.addAttribute(ATTR_THREAD, thread);
            model.addAttribute(ATTR_UPDATE_TIMEOUT, updateTimeout);
            model.addAttribute(ATTR_FORM_TITLE, String.format(HTML_FORM_TITLE,
                    utilService.getMessage(TITLE_REPLY_THREAD),
                    thread.getOpeningPost().getId(),
                    utilService.getMessage(TITLE_REPLY_MESSAGE)));
            model.addAttribute(ATTR_TITLE, getTitle(thread));

            model.addAttribute(ATTR_MODE_SELECTOR, getModeSelector(openingPostId));

            mapping = MAPPING_THREAD;
        }

        utilService.noCacheResponse(response);

        return mapping;
    }

    @GetMapping(URL_GALLERY)
    public String getGallery(@PathVariable Long openingPostId, @ModelAttribute(ATTR_FORM) PostForm form, Model model,
                            HttpServletResponse response) throws NotFoundException {
        String mapping;
        Post threadPost = postService.findById(openingPostId);
        if (threadPost == null) {
            throw new NotFoundException("Thread not found");
        } else if (!threadPost.isOpening()) {
            mapping = UrlBasedViewResolver.REDIRECT_URL_PREFIX + postService.getUrl(threadPost);
        } else {
            PostThread thread = threadPost.getThread();
            model.addAttribute(ATTR_THREAD, thread);
            model.addAttribute(ATTR_ATTACHMENTS, attachmentService.findByThread(thread));
            model.addAttribute(ATTR_TITLE, getTitle(thread));

            model.addAttribute(ATTR_MODE_SELECTOR, getModeSelector(openingPostId));

            mapping = MAPPING_GALLERY;
        }

        return mapping;
    }

    @PostMapping(URL_THREAD)
    public String createPost(@PathVariable Long openingPostId, @Valid @ModelAttribute(ATTR_FORM) PostForm form,
            BindingResult bindingResult, Model model, HttpServletRequest request,
            HttpServletResponse response) throws BusinessException {
        if (bindingResult.hasErrors()) {
            return getThread(openingPostId, form, model, response);
        } else {
            PostThread thread = postService.findById(openingPostId).getThread();
            PostCreateDetails details = postService.getCreateDetailsBuilder()
                    .address(utilService.getIpFromRequest(request))
                    .form(form)
                    .thread(thread)
                    .build();
            Post post = postService.create(details);
            return UrlBasedViewResolver.REDIRECT_URL_PREFIX + postService.getUrl(post);
        }
    }

    private String getModeSelector(long openingPostId) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(HTML_THREAD_MODE, "/thread/" + openingPostId, utilService.getMessage("thread.mode.thread")));
        sb.append(DELIMITER_MODE);
        sb.append(String.format(HTML_THREAD_MODE, "/thread/" + openingPostId + "/gallery", utilService.getMessage("thread.mode.gallery")));
        return sb.toString();
    }

    private String getTitle(PostThread thread) {
        return postService.getShortTitle(thread.getOpeningPost());
    }

}
