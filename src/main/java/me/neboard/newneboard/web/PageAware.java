package me.neboard.newneboard.web;

public interface PageAware {
    String ATTR_PAGE = "page";
}
