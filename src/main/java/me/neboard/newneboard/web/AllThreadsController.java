package me.neboard.newneboard.web;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.NotFoundException;
import me.neboard.newneboard.form.PostForm;
import me.neboard.newneboard.form.ThreadForm;
import me.neboard.newneboard.form.validation.ThreadFormValidator;
import me.neboard.newneboard.service.*;
import me.neboard.newneboard.service.impl.PostCreateDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class AllThreadsController implements PostAwareController, FormAware, PageAware {
    public static final String URL_ALL_THREADS = "/";
    public static final String URL_TAG = "/tag/";

    private static final String URL_TAG_THREADS = URL_TAG + "{tagName}";

    private static final String URL_TAG_FAV_ADD = "/tag/add-favorite";
    private static final String URL_TAG_FAV_REMOVE = "/tag/remove-favorite";

    private static final String URL_TAG_HIDDEN_ADD = "/tag/add-hidden";
    private static final String URL_TAG_HIDDEN_REMOVE = "/tag/remove-hidden";

    private static final String ATTR_THREADS = "threads";
    private static final String ATTR_TAG = "tag";
    private static final String ATTR_FAVORITE = "favorite";
    private static final String ATTR_HIDDEN = "hidden";
    private static final String ATTR_TODAY_OPENINGS = "todayOpeningPosts";

    private static final String MAPPING_ALL_THREADS = "all_threads";

    @Value("${threads.per.page}")
    private int threadsPerPage;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private PostService postService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private ThreadFormValidator validator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        if (binder.getTarget() instanceof PostForm) {
            binder.setValidator(validator);
        }
    }

    @GetMapping(URL_ALL_THREADS)
    public String getAllThreads(@ModelAttribute(ATTR_FORM) ThreadForm form,
                                @RequestParam Optional<Integer> page,
                                @RequestParam Optional<Integer> fromId,
                                Model model) {
        Page<PostThread> threadsPage = threadService.findAll(
                utilService.getPageable(page, threadsPerPage));

        templateFromExistingThread(form, fromId);

        return renderThreads(model, threadsPage, null, !page.isPresent());
    }

    private void templateFromExistingThread(@ModelAttribute(ATTR_FORM) ThreadForm form, @RequestParam Optional<Integer> fromId) {
        if (fromId.isPresent()) {
            Post existingOpening = postService.findById(fromId.get());
            form.setTitle(existingOpening.getTitle() + " NEW");
            form.setTags(existingOpening.getThread().getTags().stream()
                    .map(Tag::getName)
                    .collect(Collectors.joining(ThreadForm.DELIMITER_FORM)));
            form.setText(Post.REFLINK_START + existingOpening.getId());
        }
    }

    private String renderThreads(Model model, Page page, Tag tag, boolean includeActiveThreads) {
        if (includeActiveThreads) {
            model.addAttribute(ATTR_TODAY_OPENINGS, postService.findTodayActiveThreads(tag));
        }

        model.addAttribute(ATTR_THREADS, page);
        model.addAttribute(ATTR_PAGE, page);
        return MAPPING_ALL_THREADS;
    }

    @GetMapping(URL_TAG_THREADS)
    public String getTagThreads(@PathVariable String tagName, @ModelAttribute(ATTR_FORM) ThreadForm form, @RequestParam Optional<Integer> page, Model model) throws NotFoundException {
        Tag tag = tagService.findByName(tagName);

        if (tag == null) {
            throw new NotFoundException("Cannot find tag");
        }
        
        Page<PostThread> tagThreads = threadService.findByTag(tag, utilService.getPageable(page, threadsPerPage));

        model.addAttribute(ATTR_TAG, tag);
        model.addAttribute(ATTR_FAVORITE, settingsService.isTagFavorite(tag));
        model.addAttribute(ATTR_HIDDEN, settingsService.isTagHidden(tag));

        form.setTags(tagName);

        return renderThreads(model, tagThreads, tag, !page.isPresent());
    }

    @PostMapping(URL_ALL_THREADS)
    public String createThread(@Valid @ModelAttribute(ATTR_FORM) ThreadForm form, BindingResult bindingResult,
                               @RequestParam Optional<Integer> page, Model model, HttpServletRequest request) throws BusinessException {
        if (bindingResult.hasErrors()) {
            return getAllThreads(form, page, Optional.empty(), model);
        } else {
            PostCreateDetails details = postService.getCreateDetailsBuilder()
                    .address(utilService.getIpFromRequest(request))
                    .form(form)
                    .build();
            Post post = postService.create(details);
            return UrlBasedViewResolver.REDIRECT_URL_PREFIX + postService.getUrl(post);
        }
    }

    @PostMapping(URL_TAG_FAV_ADD)
    public String addFavTag(@RequestParam String tagName) {
        Tag tag = tagService.findByName(tagName);
        settingsService.addFavoriteTag(tag);
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX + tagService.getUrl(tag);
    }

    @PostMapping(URL_TAG_FAV_REMOVE)
    public String removeFavTag(@RequestParam String tagName) {
        Tag tag = tagService.findByName(tagName);
        settingsService.removeFavoriteTag(tag);
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX + tagService.getUrl(tag);
    }

    @PostMapping(URL_TAG_HIDDEN_ADD)
    public String hideTag(@RequestParam String tagName) {
        Tag tag = tagService.findByName(tagName);
        settingsService.addHiddenTag(tag);
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX + tagService.getUrl(tag);
    }

    @PostMapping(URL_TAG_HIDDEN_REMOVE)
    public String unhideTag(@RequestParam String tagName) {
        Tag tag = tagService.findByName(tagName);
        settingsService.removeHiddenTag(tag);
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX + tagService.getUrl(tag);
    }

}
