package me.neboard.newneboard.repository;

import me.neboard.newneboard.domain.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import me.neboard.newneboard.domain.PostThread;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Repository
public interface ThreadRepository extends JpaRepository<PostThread, Long> {
    Page<PostThread> findAllByOrderByBumpTimeDesc(Pageable pageable);
    Page<PostThread> findByTagsOrderByBumpTimeDesc(Tag tag, Pageable pageable);

    List<PostThread> findByBumpTimeLessThan(Date date);

    @Query("SELECT pt FROM PostThread pt WHERE NOT EXISTS" +
            " (SELECT t FROM PostThread pt2 " +
            " JOIN pt2.tags t WHERE pt2.id = pt.id" +
            " AND t IN :hiddenTags)" +
            " ORDER BY pt.bumpTime DESC")
    Page<PostThread> findAllExcludingHiddenTags(@Param("hiddenTags") Collection<Tag> tags, Pageable pageable);

    @Query("SELECT pt FROM PostThread pt WHERE NOT EXISTS" +
            " (SELECT t FROM PostThread pt2 " +
            " JOIN pt2.tags t WHERE pt2.id = pt.id" +
            " AND t IN :hiddenTags)" +
            " AND :tag member of pt.tags" +
            " ORDER BY pt.bumpTime DESC")
    Page<PostThread> findByTagExcludingHiddenTags(@Param("tag") Tag tag, @Param("hiddenTags") Collection<Tag> tags, Pageable pageable);
}
