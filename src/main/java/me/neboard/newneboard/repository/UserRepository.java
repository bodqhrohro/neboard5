package me.neboard.newneboard.repository;

import me.neboard.newneboard.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, QueryByExampleExecutor<User> {
    User findByUuid(String uuid);
    long deleteByLastOperatedTimeLessThan(Date date);
}
