package me.neboard.newneboard.exception;

public class ExternalRequestException extends BusinessException {
    public ExternalRequestException(String message) {
        super(message);
    }

    public ExternalRequestException(Throwable cause) {
        super(cause);
    }
}
