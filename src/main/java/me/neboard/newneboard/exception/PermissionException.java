package me.neboard.newneboard.exception;

public class PermissionException extends BusinessException {
    public PermissionException(String message) {
        super(message);
    }

    public PermissionException(Throwable cause) {
        super(cause);
    }
}
