FROM openjdk:14-slim
EXPOSE 8000
COPY ./build/libs/newneboard-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]